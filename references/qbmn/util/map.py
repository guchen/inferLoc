# -*- coding: utf-8 -*-
from __future__ import print_function
from multiprocessing import cpu_count, TimeoutError, Pool
from tqdm import tqdm


def pmap(task, iterable, chunksize=None, workers=-1, total=None):
    """intergration of pool.Map with progress display

    Args:
        task: the task to do
        iterable: parameters for the task

    Returns:
        returns a list consisting of tuples containing the corresponding items from all iterables (a kind of transpose operation)

    >>> print(pmap(float, range(5)))
    [0.0, 1.0, 2.0, 3.0, 4.0]
    >>> print(pmap(float, range(5),workers=1))
    [0.0, 1.0, 2.0, 3.0, 4.0]
    """
    if workers == 0 or workers == 1:
        # no need to use multi-processing
        return map(task, tqdm(iterable, total=total))

    if workers == -1:
        workers = cpu_count()
    pool = Pool(workers)

    with tqdm(total=total) as progress:
        try:
            if chunksize:
                rs_iter = pool.imap(
                    task, iterable, chunksize=chunksize)
            else:
                rs_iter = pool.imap(task, iterable)

            pool.close()

            # waiting for results
            rs = []

            while 1:
                try:
                    rs.append(rs_iter.next(0.5))
                    progress.update(1)
                except StopIteration:
                    break
                except TimeoutError:
                    continue

            pool.join()
            del pool
            return rs

        except KeyboardInterrupt:
            print('Main Ctrl+C')
            pool.terminate()
            pool.join()
            del pool
            exit(0)


if __name__ == '__main__':
    import doctest

    doctest.testmod(report=True)
