# -*- coding: utf-8 -*-
# Tools used to predict, Sklearn style.
from __future__ import print_function, division
from sklearn.preprocessing import LabelEncoder, normalize
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils import check_X_y, check_array
from sklearn.utils.multiclass import unique_labels, _check_partial_fit_first_call
from sklearn.metrics import accuracy_score
import numpy as np
from numba import jit, vectorize, float32, float64, guvectorize


def timeseries2XY(time_series, order):
    """
    Convert a 1-D time sequence to the style of X,Y, used in scikit-learn

    Parameters
    ----------
    time_series : list or 1-D array
    order : columns of X

    Returns
    -------
    X : array (-1,order)
    Y : 1-D array

    >>> timeseries2XY([1,2,3,4,5],1)[0]
    [[1], [2], [3], [4]]
    >>> timeseries2XY([1,2,3,4,5],1)[1]
    [2, 3, 4, 5]
    """
    X = []
    Y = []

    N = len(time_series)
    for i in range(order, N):
        x = time_series[i - order:i]
        y = time_series[i]
        X.append(x)
        Y.append(y)
    return X, Y


class BaseMarkovPredictor(BaseEstimator, ClassifierMixin):
    def __init__(self, order):
        self.order = order
        self.encoder = None
        self.transition_ = None
        self._validate_params()

    def _validate_params(self):
        if not self.order > 0:
            raise ValueError('order muse be >0')

    def _validate_input(self, X, y):
        X, y = check_X_y(X, y, ensure_min_features=self.order)
        if not X.shape[1] == self.order:
            raise ValueError('Columns of X must be equal to order')
        return X, y

    def _validate_input_X(self, X):
        X = check_array(X, ensure_min_features=self.order)
        if not X.shape[1] == self.order:
            raise ValueError('Columns of X must be equal to order')
        return X

    def _transform(self, np_array):
        shape = np_array.shape
        return self.encoder.transform(np_array.reshape(-1, )).reshape(shape)

    def _inverse_transform(self, np_array):
        shape = np_array.shape
        return self.encoder.inverse_transform(np_array.reshape(-1, )).reshape(shape)

    def _partial_fit(self, X, y, classes=None, _refit=False):
        X, y = self._validate_input(X, y)

        if _refit:
            self.classes_ = None

        if _check_partial_fit_first_call(self, classes):
            # This is the first call to partial_fit:
            n_classes = len(self.classes_)
            self.transition_counts_ = np.zeros(
                [n_classes for i in range(self.order + 1)])
            encoder = LabelEncoder().fit(self.classes_)
            self.encoder = encoder

        # convert any classess to int labels: 0,1,2,...
        X = self._transform(X)
        y = self._transform(y)

        n_samples = X.shape[0]

        for i in range(n_samples):
            self.transition_counts_[tuple(X[i])][y[i]] += 1.

        return self._build_transition()

    def _build_transition(self):
        raise NotImplemented('Should Implement Build Transition Method!')

    def fit(self, X, y):
        X, y = self._validate_input(X, y)
        labels_X = np.unique(X.reshape(-1, ))
        labels_y = np.unique(y)
        classes = np.union1d(labels_X, labels_y)
        return self._partial_fit(X, y, _refit=True, classes=classes)

    def partial_fit(self, X, y, classes=None):
        return self._partial_fit(X, y, classes=classes)

    def predict(self, X):
        X = self._validate_input_X(X)
        probabilities = self._predict_likelihood(X)
        n_samples = probabilities.shape[0]
        y = [None for i in range(n_samples)]
        for i in range(n_samples):
            counts = probabilities[i, :]
            if np.sum(counts) > 0:
                y[i] = np.argmax(counts)
            else:
                x = self._transform(X[i, :])
                y[i] = x[-1]

        y = np.array(y)
        y = self._inverse_transform(y)
        return y

    def _predict_likelihood(self, X):
        X = self._validate_input_X(X)

        X = self._transform(X)
        probabilities = []
        for i in range(X.shape[0]):
            pos = tuple(X[i])
            probabilities.append(self.transition_[pos])

        return np.array(probabilities)

    def dynamic_score(self, X, y):
        X, y = self._validate_input(X, y)
        n_samples = X.shape[0]
        y_predict = [None for i in range(n_samples)]
        for i in range(n_samples):
            y_predict[i] = self.predict([X[i]])[0]
            self.partial_fit([X[i]], [y[i]])
        return accuracy_score(y_predict, y)


class MaximumLikelihoodMarkovPredictor(BaseMarkovPredictor):
    def _build_transition(self):
        self.transition_ = self.transition_counts_
        return self


@jit(cache=True, nopython=True, nogil=True)
def n_free_param(n_classes, order):
    return np.power(n_classes, order) * (n_classes - 1)


@jit
def Gamma(n):
    return factorial(n - 1)


@vectorize(target='cpu')
def logGamma(n):
    if n < 1:
        return 0.
    elif n == 1 or n == 2:
        return 0.
    elif n == 3:
        return np.log(2)
    else:
        ns = np.arange(2, n - 1)
        return np.sum(np.log(ns))


@jit(nopython=True, nogil=True)
def alpha(n):
    return n + 1


def _logP_helpfunc(counts_sk):
    n_sk = np.sum(counts_sk)
    if n_sk > 0:
        logP = 0.0
        logP += logGamma(alpha(n_sk))
        # logP += -np.sum([logGamma(alpha(n_sks)) for n_sks in counts_sk])
        # logP += -np.sum(logGammaVector(alpha(counts_sk)))
        logP += -np.sum(logGamma(alpha(counts_sk)))
        # logP += np.sum([logGamma(n_sks + alpha(n_sks)) for n_sks in counts_sk])
        # logP += np.sum(logGammaVector(counts_sk + alpha(counts_sk)))
        logP += np.sum(logGamma(counts_sk + alpha(counts_sk)))
        logP += -logGamma(n_sk + alpha(n_sk))
        return logP
    else:
        return 0.0


@guvectorize(["void(float64[:],float64[:])", "void(float32[:],float32[:])"], '(n)->()', target='cpu', nopython=True)
def _logP_helpfunc_v(counts_sk, out):
    # If one of the arguments should be a scalar, the corresponding layout
    # specification is () and the argument will really be given to you as a
    # zero-dimension array (you have to dereference it to get the scalar
    # value)
    logP = 0.0
    n_sk = np.sum(counts_sk)
    if n_sk > 0:
        logP += logGamma(alpha(n_sk))
        # logP += -np.sum([logGamma(alpha(n_sks)) for n_sks in counts_sk])
        # logP += -np.sum(logGammaVector(alpha(counts_sk)))
        logP += -np.sum(logGamma(alpha(counts_sk)))
        # logP += np.sum([logGamma(n_sks + alpha(n_sks)) for n_sks in counts_sk])
        # logP += np.sum(logGammaVector(counts_sk + alpha(counts_sk)))
        logP += np.sum(logGamma(counts_sk + alpha(counts_sk)))
        logP += -logGamma(n_sk + alpha(n_sk))

    out[0] = float32(logP)


@jit
def logP_D_given_Mk(transition_counts):
    logP = 0.0
    n_classes = transition_counts.shape[0]
    _arr_counts = transition_counts.reshape(-1, n_classes)

    logP += np.sum(_logP_helpfunc_v(_arr_counts))
    return logP


def logP_Mk_given_D_M(transition_counts):
    n_classes = transition_counts.shape[0]
    order = len(transition_counts.shape) - 1

    logP = logP_D_given_Mk(transition_counts) - n_free_param(n_classes, order)

    return logP


def compute_markov_best_order(time_series, orders=[1, 2, 3, 4]):
    """
    >>> compute_markov_best_order(np.random.randint(0,1,1000))
    1
    """
    bestOrder = None
    bestLogP = -np.inf
    for order in orders:
        X, Y = timeseries2XY(time_series, order)
        clf = MaximumLikelihoodMarkovPredictor(order=order)
        clf = clf.fit(X, Y)

        transition_counts = clf.transition_counts_
        logP = logP_Mk_given_D_M(transition_counts)
        if logP > bestLogP:
            bestOrder = order
            bestLogP = logP
    return bestOrder


try:
    from scipy.misc import factorial
    import pymc3 as pm
    from theano import tensor as tt


    def buildPyMC3MarkovModel(transition_counts, alpha=None):
        if alpha is None:
            alpha = np.ones(
                transition_counts.shape[0]) / 2.  # alpha=0.5 by default
        elif len(alpha) != transition_counts.shape[0]:
            raise EnvironmentError(
                'alpha should be equal to the classes in transition_counts_')

        with pm.Model() as model:
            # define model
            P = tt.constant(1.0)
            for i in np.ndindex(transition_counts.shape[0:-1]):
                if transition_counts[i].sum() > 0:
                    pi = pm.Dirichlet('P%s' % str(i), a=alpha)
                    P *= pm.Multinomial('counts%s' % str(i), p=pi,
                                        n=transition_counts[i].sum(), observed=transition_counts[i])
        return model


    class MaximumAPosterioriMarkovPredictor(BaseMarkovPredictor):
        def _build_transition(self):
            model = buildPyMC3MarkovModel(self.transition_counts_)
            self.model = model
            MAP_estimate = pm.find_MAP(model=model, disp=False)
            transition = np.zeros(self.transition_counts_.shape)
            for i in np.ndindex(transition.shape[0:-1]):
                if self.transition_counts_[i].sum() > 0:
                    pi = model['P%s' % str(i)]
                    ti = pi.transformation.backward(pi).eval(
                        {pi.owner.inputs[0]: MAP_estimate['P%s_stickbreaking__' % str(i)]})
                    transition[i] = ti
            self.transition_ = transition
            # print self.transition_
            return self


    class MCMCMarkovPredictor(BaseMarkovPredictor):
        def _build_transition(self):
            model = buildPyMC3MarkovModel(self.transition_counts_)
            self.model = model
            transition = np.zeros(self.transition_counts_.shape)
            with model:
                start = pm.find_MAP(disp=False)
                trace = pm.sample(init=None, start=start, progressbar=False)

            for i in np.ndindex(transition.shape[0:-1]):
                if self.transition_counts_[i].sum() > 0:
                    ti = trace['P%s' % str(i)].mean()
                    transition[i] = ti
            self.transition_ = transition
            # print self.transition_
            return self

except Exception:
    print("Init predictor: fail to load pymc3.")

if __name__ == '__main__':
    import doctest

    doctest.testmod()
