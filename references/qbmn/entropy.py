# -*- coding: utf-8 -*-
from __future__ import print_function, division
import numpy as np
# use log_2 as logarithm when computing entropy.
from numpy import log2 as log
from functools import wraps
from numba import jit
import warnings

print('Init module entropy: Logarithm function of entropy is %s' % log.__name__)


def entropy_rand(a):
    """
    Compute random entropy

    Equation:
    $H_{rand} = \log{|X|}$, where $|X|$ is the alphabet of random variable X.

    Parameters
    ----------
    a : 1-D array-like
        samples of a random variable

    Returns
    -------
    etp_rand : float
        random entropy of X

    >>> entropy_rand([1,2,3,4]) - log(4)
    0.0
    >>> entropy_rand([1,1,1,1,1])
    0.0
    """
    size_alphabet = np.unique(a).size
    etp_rand = log(size_alphabet)
    return etp_rand


@jit
def _entropy_tunc_counts(counts):
    probabilities = counts / counts.sum()
    etp_tunc = 0.0 - np.sum(probabilities * log(probabilities))
    return etp_tunc


def entropy_tunc(a):
    """
    Compute temporal-uncorrelated entropy

    Equation:
    $H_{unc} = - \sum p_i \log p_i$

    Parameters
    ----------
    a : 1-D array-like
        samples of a random variable

    Returns
    -------
    etp_tunc : float
        temporal-uncorrelated entropy of X

    >>> entropy_tunc([1,1,1,1,1])
    0.0
    >>> entropy_tunc([1,2,3,4]) - (-4*0.25*log(0.25))
    0.0
    """
    _, counts = np.unique(a, return_counts=True)
    etp_tunc = _entropy_tunc_counts(counts)
    return etp_tunc


def probability_max(a):
    """
    Highest probability of random variable in X. Be considered as "regularity" in [1]

    Equation:
    $P_{max} = \max \Pr(X=x)$

    [1] Song, Chaoming, et al. "Limits of predictability in human mobility." Science 327.5968 (2010): 1018-1021.

    Parameters
    ----------
    a : 1-D array-like
        samples of a random variable

    Returns
    -------
    p_max : float
        the highest probability

    >>> type(probability_max([]))
    <type 'NoneType'>
    >>> probability_max([1,1,1,1,1])
    1.0
    >>> probability_max([1,2,3,4])
    0.25
    """
    _, counts = np.unique(a, return_counts=True)
    return np.max(counts / counts.sum()) if counts.size > 0 else None


def _lambda(seq, pos, last=0):
    """
    Compute lambda in estimating entropy rate (python, slow)

    Compute the length of the shortest substring starting at position i
    which doesn't previously appear from position 1 to i-1.
    """
    n = len(seq)
    before = seq[0:pos]
    # _SEP = '|*|'
    # str_before = _SEP.join(map(str, before))

    # optimized
    end = pos + 1 if last <= 2 else pos + last - 1
    while end < n + 1:
        after = seq[pos:end]
        # str_after = _SEP.join(map(str, after))
        if not _arr_in_arr(after, before):
            # if not (str_after in str_before):
            return end - pos
        else:
            end += 1
    return 0


def _arr_in_arr(pattern, search):
    l = len(pattern)
    s = len(search)
    if l == 0:
        return True
    if s == 0:
        return False
    if l > s:
        return False

    for p in range(s - l + 1):
        if search[p] == pattern[0] and tuple(search[p:p + l]) == tuple(pattern):
            return True
    return False


def _lz(seq):
    # Estimated entropy rate using the Lempel-Ziv data compression
    # item in seq should be able to str() uniformly.
    # etp_rate = log(n) / (_sum * 1.0 / n)
    # to have a united logarithm, only return n, _sum
    n = len(seq)
    _sum = 1.0
    last = 1.0  # i=0
    for i in range(1, n):
        last = _lambda(seq, i, last)
        if last == 0:
            break
        else:
            _sum += last
    return n, _sum


try:
    import pyximport

    pyximport.install()
    from LZ import lz as _lz_pyx

    _lz = _lz_pyx
except Exception:
    print("Init module entropy: Failed to load cython for optimizing entropy_rate. Use slow version instead.")


def entropy_rate(seq):
    """
    Compute entropy rate (or actual entropy in [1])

    Entropy rate is calculated by an estimator based on Lempel-Ziv data compression.
    Equation:
    $H_{rate} = (1/n \sum_i \Lambda_i )^{-1} \log n$
    where
        $n$: the length of time sequence (number of samples),
        $\Lambda$: the length of the shortest substring starting at position i
            which does not previously appear from position 1 to i-1.

    [1] Song, Chaoming, et al. "Limits of predictability in human mobility." Science 327.5968 (2010): 1018-1021.
    [2] Online Supportive document of [1].

    Parameters
    ----------
    seq : 1-D array-like
        a time sequence of samples of a random variable

    Returns
    -------
    etp_rate : float
        entropy rate

    >>> entropy_rate([1,2,3,4,5]) - log(5)*(5/5)**(-1)
    0.0
    >>> entropy_rate(range(3000)+range(3000)) - log(6000)*(6000/3000)
    0.0
    """

    alphabet, seq_int = np.unique(seq, return_inverse=True)

    if len(alphabet) < 0x1000:
        # x in unichr(x) shall be in range(0x10000) (narrow Python build)
        seq_str = u''.join(map(unichr, seq_int))
        etp_rate = _entropy_rate_str(seq_str)
        return etp_rate
    else:
        n, _sum = _lz(seq_int)
        etp_rate = log(n) / (_sum * 1.0 / n)
        return etp_rate
        #
        # # 1. try to convert and check if n_states < 255
        # convert_table = {}
        # seq_int_arr = []
        #
        # i = 0
        # for x in seq:
        #     if x in convert_table:
        #         seq_int_arr.append(convert_table[x])
        #     else:
        #         convert_table[x] = i
        #         seq_int_arr.append(i)  # equal to seq_int_arr.append(convert_table[x])
        #         i += 1
        #         if i > 254:
        #             # 2. i > 254 means that n_states >= 255. So use slow version.
        #             n, _sum = _lz(seq)
        #             etp_rate = log(n) / (_sum * 1.0 / n)
        #             return etp_rate
        #
        # # 3. if seq can be converted to seq_str, then use _entropy_rate_str to have better performance.
        # seq_str = ''.join(map(chr, seq_int_arr))
        # etp_rate = _entropy_rate_str(seq_str)
        # return etp_rate


def _entropy_rate_str(seq_str):
    """
    Compute entropy rate (python version, fast). See entropy_rate.

    Parameters
    ----------
    seq_str : str
        a time sequence of samples. Given in a string. Each char represents a state.

    Returns
    -------
    etp_rate: float
        entropy rate
    """
    n = len(seq_str)
    _sum = 1.0
    lam = 1.0
    for i in xrange(1, n):
        # get lambda
        before = seq_str[:i]
        end = i + 1 if lam <= 2 else i + lam - 1
        lam_next = 0
        while end < n + 1:
            after = seq_str[i:end]
            if after not in before:
                lam_next = end - i
                break
            else:
                end += 1
        lam = lam_next
        # end
        if lam == 0:
            break
        else:
            # print lam
            _sum += lam

    etp_est = log(n) / (_sum * 1.0 / n)
    return etp_est


def compute_entropy_related_stats(a, unknown_mask=0, func_etp_rate=entropy_rate):
    """
    Compute statistics that relate to entropy.

    Some variables are shared when computing entropy, in order to save time.

    Parameters
    ----------
    a : 1-D array-like
        samples of a random variable
    unknown_mask: single hashable item, optional
        a value representing "unknown". Default: 0
    func_etp_rate: function
        function to compute entropy rate. Defaut: entropy_rate

    Returns
    -------
    stat: dict
        Statistics, including:
        1. etp_rand: random entropy
        2. etp_unc: temporal-uncorrelated entropy
        3. etp_unc_pure: temporal-uncorrelated entropy of samples ignoring unknown mark.
        4. etp_est: actual entropy (entropy rate)
        5. np: size of alphabet
        6. P_{state}: probabilities of states

    >>> compute_entropy_related_stats([1,2,3,4])['N']
    4
    >>> compute_entropy_related_stats([1,2,3,4])['etp_rand'] - entropy_rand([1,2,3,4])
    0.0
    >>> compute_entropy_related_stats([1,2,3,4])['etp_unc'] - entropy_tunc([1,2,3,4])
    0.0
    >>> compute_entropy_related_stats([0,1,2,3,4])['etp_unc_pure'] - entropy_tunc([1,2,3,4])
    0.0
    >>> compute_entropy_related_stats([0,1,2,3,4,5])['etp_est'] - entropy_rate([0,1,2,3,4,5])
    0.0
    >>> compute_entropy_related_stats([1,2,3,4])['P_1']
    0.25
    """

    states, counts = np.unique(a, return_counts=True)
    probabilities = counts / counts.sum()
    probabilities_dict = {}
    for i in range(states.shape[0]):
        state = states[i]
        probability = probabilities[i]
        probabilities_dict['P_' + str(state)] = probability

    N = states.size
    etp_rand = log(N)
    etp_unc = _entropy_tunc_counts(counts)
    counts_pure_states = counts[states != unknown_mask]
    etp_unc_pure = _entropy_tunc_counts(counts_pure_states)
    etp_est = func_etp_rate(a)

    return dict({
        'etp_rand': etp_rand,
        'etp_unc': etp_unc,
        'etp_est': etp_est,
        'etp_unc_pure': etp_unc_pure,
        'N': N,
    }, **probabilities_dict)


def _optimize_predictability_cal(predictability_cal_func):
    # wrap to add cache on results of func(S,Nnp
    # cache key (round(S,2),int(Nnp)
    cache = {}

    @wraps(predictability_cal_func)
    def opt_predictability_cal(etp_float, n_alphabet):
        etp_float = round(etp_float, 2)
        n_alphabet = int(n_alphabet)
        if (etp_float, n_alphabet) in cache:
            return cache[(etp_float, n_alphabet)]
        else:
            P = predictability_cal_func(etp_float, n_alphabet)
            cache[(etp_float, n_alphabet)] = P
            return P

    return opt_predictability_cal


@_optimize_predictability_cal
def predictability_max(etp, n_alphabet):
    """
    Compute an upper bound of the maximum predictability [1].

    Equation:
    $H = -\Pi \log \Pi - (1-\Pi) \log \frac{1-\Pi}{Nnp1} $
    where
        $\Pi$: upper bound of maximum predictability
        $H$: entropy
        $Nnp: size of alphabet

    [1]	M. Feder and Nnp Merhav, "Relations between entropy and error probability",
        IEEE Trans. Inform. Theory, vol. 40, no. 1, pp. 259–266, 1994.

    Parameters
    ----------
    etp : float
        entropy or entropy rate of X
    n_alphabet :
        size of the alphabet of X

    Returns
    -------
    pred_max: float
        upper bound of maximum predictability. Precision: 1e-3

    >>> predictability_max(0,0)
    0.0
    >>> predictability_max(0,1)
    1.0
    >>> predictability_max(entropy_rand([1,2]),2)
    0.5
    """
    if n_alphabet == 1:
        return 1.
    elif n_alphabet == 0:
        return 0.

    if etp == 0:
        return 1.

    xs = np.arange(1.0 / n_alphabet, 1., 0.001)

    H = -xs * log(xs) - (1 - xs) * log(1 - xs)
    F = etp - H - (1 - xs) * log(n_alphabet - 1)
    ys = np.power(F, 2)
    # end

    pos = np.argmin(ys)

    return round(xs[pos], 3)


@np.vectorize
@jit(nopython=True)
def _cal_err(p_err, n_alphabet, etp):
    # (12) in [1]
    # find
    i = 1.0
    while i <= n_alphabet - 1:
        if (i - 1) / i <= p_err <= i / (i + 1):
            break
        else:
            i += 1.0

    p1 = (1 - p_err) * i * log(i)
    alpha = i * p_err - (i - 1)
    p2 = -alpha * log(alpha) - (1 - alpha) * log(1 - alpha)
    return p1 + p2 - etp


@_optimize_predictability_cal
def predictability_min(etp, n_alphabet):
    """
    Compute an lower bound of the maximum predictability [1], when input is entropy (not rate).

    Equation:
    (12)(13) in [1]

    [1]	M. Feder and Nnp Merhav, "Relations between entropy and error probability",
        IEEE Trans. Inform. Theory, vol. 40, no. 1, pp. 259–266, 1994.

    Parameters
    ----------
    etp : float
        entropy of X
    n_alphabet : int
        size of the alphabet of X

    Returns
    -------
    pred_min: float
        lower bound of maximum predictability. Precision: 1e-3

    >>> predictability_min(0,0)
    0.0
    >>> predictability_min(0,1)
    1.0
    >>> predictability_min(0,8)
    1.0
    >>> predictability_min(3,8)
    0.125
    """

    if n_alphabet == 1:
        return 1.
    elif n_alphabet == 0:
        return 0.

    if etp == 0:
        return 1.

    p_err_candicates = np.arange(0.001, 1.0 * (n_alphabet - 1) / n_alphabet + 0.001, 0.001)
    errors = np.power(_cal_err(p_err_candicates, n_alphabet, etp), 2)
    pred_min = 1 - p_err_candicates[np.argmin(errors)]
    return round(pred_min, 3)


@np.vectorize
@jit(nopython=True)
def _cal_err_erate(p_err, n_alphabet, etp_rate):
    # (14) in [1]
    # find i
    i = 1.0
    while i <= n_alphabet - 1:
        if (i - 1) / i <= p_err <= i / (i + 1):
            break
        else:
            i += 1.0
    a = i * (i + 1) * log((i + 1) / i)
    b = log(i)
    return a * (p_err - (i - 1) / i) + b - etp_rate


@_optimize_predictability_cal
def predictability_min_erate(etp_rate, n_alphabet):
    """
    Compute an lower bound of the maximum predictability [1], when input is entropy rate(not entropy).

    Equation:
    (14)(17) in [1]

    [1]	M. Feder and n_alphabet. Merhav, "Relations between entropy and error probability",
        IEEE Trans. Inform. Theory, vol. 40, no. 1, pp. 259–266, 1994.

    Parameters
    ----------
    etp_rate : float
        entropy rate of X
    n_alphabet :
        size of the alphabet of X

    Returns
    -------
    pred_min: float
        lower bound of maximum predictability. Precision: 1e-3

    >>> predictability_min_erate(0,0)
    0.0
    >>> predictability_min_erate(0,1)
    1.0
    >>> predictability_min_erate(0,8)
    1.0
    >>> predictability_min_erate(1,8)
    0.5
    >>> predictability_min_erate(3,8)
    0.125
    >>> predictability_min_erate(0.5,8)
    0.75
    >>> predictability_min_erate(0.75,8) < predictability_min(0.75,8)
    True
    """
    if n_alphabet == 1:
        return 1.
    elif n_alphabet == 0:
        return 0.

    if etp_rate == 0:
        return 1.

    p_err_candicates = np.arange(0.001, 1.0 * (n_alphabet - 1) / n_alphabet + 0.001, 0.001)
    errors = np.power(_cal_err_erate(p_err_candicates, n_alphabet, etp_rate), 2)
    pred_min = 1 - p_err_candicates[np.argmin(errors)]
    return round(pred_min, 3)


# TODO remove

@jit
def _cal_etp_tunc_via_permutation(seq, seed):
    np.random.seed(seed)
    _seq = np.random.permutation(seq)
    etp_tunc = entropy_rate(_seq)
    return etp_tunc


@jit
def _entropy_tunc_by_erate(seq):
    """
    Estimate temporal-uncorrelated entropy. Used in entropy_rate_location.

    Unlike entropy_tunc, the entropy is estimated by randomly permutating seq
        and then computing its entropy rate.

    Parameters
    ----------
    seq : 1-D array-like
        samples of a random variable

    Returns
    -------
    etp_tunc : float
        temporal-uncorrelated entropy
    """

    seeds = np.arange(10)
    func = np.vectorize(_cal_etp_tunc_via_permutation, signature='(n),()->()')
    etp_tunc_q = np.max(func(seq, seeds))
    # etp_tunc_q = max([_cal_etp_tunc_via_permutation(seq, seed) for seed in seeds])
    return etp_tunc_q


def entropy_rate_location(seq_loc, unknown_mask='?'):
    """
    Compute entropy rate (or actual entropy in [1]), for only a time sequence of *locaitons*.

    Different from entropy_rate, the estimation is built according to S4.B in [2],
        since there are unknown locations in the time sequence via linear approaching.

    [1] Song, Chaoming, et al. "Limits of predictability in human mobility." Science 327.5968 (2010): 1018-1021.
    [2] Online Supportive document of [1].

    Parameters
    ----------
    seq_loc : 1-D array-like
        a time sequence of samples (locations) of a random variable
    unknown_mask : single hashable item, optional
        a value representing "unknown" locations. Default: '?'

    Returns
    -------
    etp_rate : float
        entropy rate

    """

    locations, counts = np.unique(seq_loc, return_counts=True)
    if unknown_mask not in locations:
        # all locations are known, use normal way
        return entropy_rate(seq_loc), 1.0

    total = counts.sum()
    probabilities = {locations[i]: counts[i] / total for i in range(locations.shape[0])}
    q = probabilities[unknown_mask]
    N = len(locations)

    seq = np.array(seq_loc)
    sunc = entropy_tunc(seq[seq != unknown_mask])

    x = []
    fx = []

    delta_q = 0.05
    for _q in np.arange(q, 0.90 + 1e-3, delta_q):
        fxq = []
        _index = np.arange(seq.shape[0])[seq != unknown_mask]
        # TODO: optimize
        for seed in np.arange(10):  # before 20
            np.random.seed(seed)
            _seq = seq.copy()
            _seq.loc[np.random.choice(_index, int(total * (q - _q)))] = unknown_mask
            _sq = entropy_rate(_seq.tolist())
            _sunc = _entropy_tunc_by_erate(_seq, N)
            if _sunc > 0:
                fxq.append(np.log(_sq / _sunc))
            else:
                warnings.warn('entropy _sunc is below 0! Something is wrong.')
                continue

        x.append(_q)
        fx.append(np.mean(fxq))
    delta = np.polyfit(x, fx, 1)[1]
    etp = (np.e ** delta) * sunc
    cov = np.corrcoef(x, fx)[0][1]
    return etp, cov


if __name__ == '__main__':
    import doctest

    doctest.testmod(report=True)

    import random
    import time

    print('Test entropy_rate on a sequence of 2200 samples')
    print('n_states', 'entropy rate', 'time exec', sep='\t')
    # 10 states
    H = [str(random.randint(0, 10)) for j in range(0, 2200)]
    t = time.time()
    print(10, entropy_rate(H), time.time() - t, sep='\t')
    # 2200 states
    H = map(str, range(2000))
    random.shuffle(H)
    t = time.time()
    print(2200, entropy_rate(H), time.time() - t, sep='\t')

    predictability_min_erate(0.5, 8)
