from qbmn.base import Analyzer
import pandas as pd


class ActiveHourAnalyzer(Analyzer):
    """
    data: should be a pandas DataFrame as
    phone as index
    sessions or sum of sessions by time
    """

    def __init__(self, data):
        super(ActiveHourAnalyzer, self).__init__(data)

    def run(self):
        dt = self.dt
        dt.columns = pd.DatetimeIndex(dt.columns)
        # resample by 1hour
        dt = dt.resample('60T', axis=1).sum().fillna(0).astype('uint32')

        # resample by 1 days and count active hour perday
        def count_active_hour(groups):
            return groups.apply(pd.np.count_nonzero, axis=1)

        self.rs = dt.resample('24H', axis=1).apply(count_active_hour)
        return self.rs

    def dump_results(self, dir_path, filename='activehour_per_user.txt.gz'):
        self.rs.to_csv(dir_path + filename, compression='gzip')
        return self.rs
