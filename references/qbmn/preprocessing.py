# -*- coding: utf-8 -*-
from __future__ import print_function, division
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import column_or_1d


class VolumeEncoder(BaseEstimator, TransformerMixin):
    """
    Quantizition Levels

    Q1: four quantization levels, i.e., idle (0 KB), light (1 KB, 1 MB), heavy (1 MB, 1 GB), and extremely heavy (1 GB, 10 GB).

    Q2: eight quantization levels, i.e., 0, (1, 10), (10, 10^2 ), . . . , (10^6, 10^7), all values in KB. Once stated otherwise, Q2 will be our default se ing.

    Q3: twelve quantization levels, obtained by bisecting each level over 1 MB in Q2, e.g., splitting (1,10) MB into (1,5.5) MB and (5.5,10) MB.

    Q4: sixteen quantization levels, obtained by trisecting each level over 1 MB in Q2.

    Q5: forty quantization levels, obtained by nine-secting each level over 1 MB in Q2.

    >>> V = [0,1,11,101,1001,1e4+1,1e5+1,1e6+1,1e7+1]
    >>> VolumeEncoder('q1').fit_transform(V).tolist()
    [0, 1, 1, 1, 2, 2, 2, 3, 3]
    >>> VolumeEncoder('q2').fit_transform(V).tolist()
    [0, 1, 2, 3, 4, 5, 6, 7, 8]
    """

    def __init__(self, mode):

        if mode not in ['q1', 'q2', 'q3', 'q4', 'q5']:
            raise Exception('Mode is wrong. Must be in q1, q2, q3, q4, q5.')
        self.mode = mode

    def fit(self, y):
        return self

    def transform(self, y):
        """Transform volumes to labels based on magnitudes.

        Parameters
        ----------
        y : array-like of shape [n_samples]
            Target values.

        Returns
        -------
        y : array-like of shape [n_samples]
        """
        y = column_or_1d(y, warn=True)
        return self._volume2label(y)

    def fit_transform(self, y):
        """Transform volumes to labels based on magnitudes.

        Parameters
        ----------
        y : array-like of shape [n_samples]
            Target values.

        Returns
        -------
        y : array-like of shape [n_samples]
        """
        return self.transform(y)

    def _volume2label(self, vols):
        orders = np.ceil(np.log10(vols + 1.))
        if self.mode == 'q1':
            return np.ceil(orders / 3).astype('uint8', copy=False)
        elif self.mode == 'q2':
            return orders.astype('uint8', copy=False)
        elif self.mode == 'q3':
            # numpy.where(c,xv,yv)
            # [xv if c else yv for (c, xv, yv) in zip(condition, x, y)]
            orders_q3 = orders * 10 + np.floor((vols - 10 ** (orders - 1)) / (9 * 10 ** (orders - 1) / 2.0))
            return np.where(vols > 1000, orders_q3, orders).astype('uint8', copy=False)
        elif self.mode == 'q4':
            orders_q4 = orders * 10 + np.floor((vols - 10 ** (orders - 1)) / (9 * 10 ** (orders - 1) / 3.0))
            return np.where(vols > 1000, orders_q4, orders).astype('uint8', copy=False)
        elif self.mode == 'q5':
            orders_q5 = orders * 10 + np.floor((vols - 10 ** (orders - 1)) / (9 * 10 ** (orders - 1) / 9.0))
            return np.where(vols > 1000, orders_q5, orders).astype('uint8', copy=False)
        else:
            raise Exception('Mode is wrong.')

    def inverse_transform(self, y):
        raise Exception('Inverse transform of volumeEncoder is impossible!')


if __name__ == '__main__':
    import doctest

    doctest.testmod()
