# -*- coding: utf-8 -*-
from __future__ import print_function, division
from functools import partial
from itertools import imap

import pandas as pd
from tqdm import tqdm
from qbmn.util import pmap


def _measure_user_features(row_args):
    # shall return pd.Series
    name = row_args[0].name
    return pd.Series({'test%d' % i: str(name) for i in range(len(row_args))}, name=name)


class IndividualAnalyzer(object):
    """
    Measure features of time series of each single user.
    """

    def __init__(self, core_dt, *dt_args):
        """
        Parameters
        ----------
        core_dt : pandas.DataFrame
            a dataset of behaviors. Index: users, Row: behavior of a user.
        dt_args : list of pandas.DataFrame
            additional datasets. On each dataset, the index must be exactly the same as in core_dt

        >>> d1 = pd.DataFrame([],index=[0,1,2,3])
        >>> d2 = pd.DataFrame([],index=[0,1,2,3])
        >>> d3 = pd.DataFrame([],index=[0,1,2,3,4])
        >>> IndividualAnalyzer(d1,d2).result is None
        True
        >>> IndividualAnalyzer(d1,d3)
        Traceback (most recent call last):
            ...
        ValueError: Each dt in dt_args must have the same index as core_dt!
        """
        self.datasets = [core_dt]
        for _dt in dt_args:
            if _dt.index.equals(core_dt.index):
                self.datasets.append(_dt)
            else:
                raise ValueError('Each dt in dt_args must have the same index as core_dt!')
        self.result = None
        self.index = core_dt.index
        self.task_func = None

    def set_task(self, task_func):
        self.task_func = task_func
        return self

    def _check_first_run(self):
        if self.result is None:
            raise EnvironmentError('No result. Please execute run fuction first!')
        if self.task_func is None:
            raise EnvironmentError('tast_func must be a function of pd.Series(list(pd.Series)).')

    def run(self, n_jobs=-1):
        """
        Analyze the features
        >>> data = pd.DataFrame([], index=[0,1,2,3])
        >>> analyzer = IndividualAnalyzer(data).set_task(_measure_user_features)
        >>> isinstance(analyzer.run(),IndividualAnalyzer)
        True
        >>> 'test0' in analyzer.result.columns
        True
        >>> print(analyzer.result['test0'].values.tolist())
        ['0', '1', '2', '3']
        >>> analyzer.result.index.equals(analyzer.index)
        True
        >>> IndividualAnalyzer(data,data,data).set_task(_measure_user_features).run().result.columns.tolist()
        ['test0', 'test1', 'test2']
        """
        result = pd.DataFrame([])
        n_users = len(self.index)

        for user_result in pmap(self.task_func,
                                (map(lambda dt: dt.iloc[i], self.datasets) for i in range(n_users))):
            result = result.append(user_result)

        self.result = result
        return self

    def dump_result(self, file_path, **kargs):
        self._check_first_run()
        compression = None
        if file_path[-3:] == '.gz':
            compression = 'gzip'
        elif file_path[-3:] == '.bz2':
            compression = 'bzip'
        self.result.to_csv(file_path, compression=compression, **kargs)
        return self.rs

    def print_info(self):
        self._check_first_run()
        print(self.result.describe())


class Analyzer(object):
    """docstring for Analyzer"""

    def __init__(self, data):
        super(Analyzer, self).__init__()
        self.dt = data

    def filter(self, filter_func):
        self.dt = filter_func(self.dt)
        return self.dt

    def run(self):
        return NotImplemented

    def dump_results(self, dir_path, filename='results.csv.gz'):
        self.rs.to_csv(dir_path + '/' + filename, compression='gzip')
        return self.rs


def _run_chunk(chunk, AnalyzerClass, filter_func, kargs):
    analyzer = AnalyzerClass(chunk) if len(kargs) == 0 else AnalyzerClass(chunk, **kargs)
    if hasattr(filter_func, '__call__'):
        analyzer.filter(filter_func)
    analyzer.run()
    return analyzer.rs


class ChunkAnalyzer(Analyzer):
    """docstring for ChunkAnalyzer"""

    def __init__(self, chunks, AnalyzerClass, pool=False, total=None, chunksize=1, **kargs):
        super(ChunkAnalyzer, self).__init__(chunks)
        # self.dt = chunks
        self.filter_func = None
        self.pool = pool
        self.AnalyzerClass = AnalyzerClass
        self.chunksize = chunksize
        self.total = total
        self.rs = pd.DataFrame([])
        self.kargs = kargs

    def filter(self, filter_func):
        self.filter_func = filter_func

    def run(self):
        print("Running:", self.AnalyzerClass.__name__)
        run_chunk = partial(
            _run_chunk, AnalyzerClass=self.AnalyzerClass, filter_func=self.filter_func, kargs=self.kargs)

        if self.pool:
            results = pmap(run_chunk, self.dt, chunksize=self.chunksize, total=self.total)
        else:
            results = tqdm(imap(run_chunk, self.dt), total=self.total)

        self.rs = None
        for rs in results:
            self.rs = rs if self.rs is None else self.rs.append(rs)
        return self.rs


__all__ = ['IndividualAnalyzer']
if __name__ == '__main__':
    import doctest
    doctest.testmod()
