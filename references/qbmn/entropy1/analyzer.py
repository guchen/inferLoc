"""
Entropy Analyzer
"""
import pandas as pd

from qbmn.base import Analyzer
from qbmn.task.predictability import measure_user_predictability_volume, measure_user_regularity_volume, measure_user_predictability_location, _compute_vunc


class EntropyAnalyzer(Analyzer):
    """docstring for EntropyAnalyzer"""

    def __init__(self, data):
        super(EntropyAnalyzer, self).__init__(data)

    def run(self):
        res = pd.DataFrame([])
        for phone, series in self.dt.iterrows():
            res[phone] = measure_user_predictability_volume(series)

        self.rs = res.T
        return self.rs

    def load_results(self, dir_path, filename='entropies_predictabilities.csv.gz'):
        rs = pd.read_csv(dir_path + '/' + filename, index_col=0)
        self.rs = rs
        return rs

    def dump_results(self, dir_path, filename='entropies_predictabilities.csv.gz'):
        return super(EntropyAnalyzer, self).dump_results(dir_path, filename)


class RegularityAnalyzer(Analyzer):
    def __init__(self, data):
        super(RegularityAnalyzer, self).__init__(data)

    def run(self):
        self.rs = pd.DataFrame([])
        for phone, series in self.dt.iterrows():
            self.rs.loc[phone] = measure_user_regularity_volume(series)
        return self.rs

    def load_results(self, dir_path, filename='regularities.csv.gz'):
        return NotImplemented

    def dump_results(self, dir_path, filename='regularities.csv.gz'):
        return super(RegularityAnalyzer, self).dump_results(dir_path, filename)


class LocationAnalyzer(Analyzer):
    def __init__(self, data):
        super(LocationAnalyzer, self).__init__(data)

    def run(self):
        # self.rs = pd.DataFrame([])
        # for phone, series in self.dt.iterrows():
        #     print phone,measure_user_predictability_location(series)
        #     self.rs.loc[phone] = measure_user_predictability_location(series)
        # return self.rs
        self.rs = self.dt.apply(measure_user_predictability_location, axis=1)
        return self.rs

    def dump_results(self, dir_path, filename='entropies_predictabilities_locations.csv.gz'):
        return super(LocationAnalyzer, self).dump_results(dir_path, filename)


class VuncAnalyzer(Analyzer):
    def __init__(self, data):
        super(VuncAnalyzer, self).__init__(data)

    def run(self):
        self.rs = pd.DataFrame([])
        for phone, series in self.dt.iterrows():
            self.rs.loc[phone] = _compute_vunc(series)
        return self.rs

    def dump_results(self, dir_path, filename='entropies_predictabilities_vunc.csv.gz'):
        return super(VuncAnalyzer, self).dump_results(dir_path, filename)
