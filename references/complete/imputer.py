import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import ParameterGrid, ParameterSampler

from complete.method import matrix_completion, tensor_completion, interpolation
from complete.util import loss, dist, rg, Parallel, delayed, clean


class BaseImputer():
    def __init__(self, C, name='base'):
        self.name = f'{name}-impute-C{C}'
        self.C = C

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        raise NotImplementedError

    def run(self, user, traj_label, data, **kwargs):
        try:
            result = self._run(user, traj_label, data, **kwargs)
            return result
        except Exception as e:
            print('Error in user:', user, 'info:', str(e))
            return {}

    def _run(self, user, traj_label, data, progress=False, **kwargs):
        result = []
        # expand
        traj_xy = data.traj2xys(traj_label)
        completion = self.C
        for traj_loss in data.loss(traj_label, C=completion):
            selected = traj_loss.isnull()
            loss_index = traj_loss[selected].index
            good_index = traj_loss[~selected].index
            # complete
            traj_est_xy = self.method(
                traj_loss, data=data, traj_orig=traj_label, **kwargs)
            # to label
            traj_est_label = data.traj2labels(traj_est_xy)
            # recover
            traj_est_label.loc[good_index] = traj_loss.loc[good_index]
            result += traj_est_label.tolist()
        return {user: result}


class FillMean(BaseImputer):
    def __init__(self, C):
        super(FillMean, self).__init__(C, 'fillmean')

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        traj_xy = data.traj2xys(traj_label)
        loss = traj_xy['x'].isnull()
        traj_xy['x'][loss] = traj_xy['x'][~loss].mean()
        traj_xy['y'][loss] = traj_xy['y'][~loss].mean()
        return traj_xy


class LastSeen(BaseImputer):
    def __init__(self, C):
        super(LastSeen, self).__init__(C, 'lastseen')

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        traj_label_est = traj_label.fillna(
            method='ffill').fillna(method='bfill')
        traj_xy_est = data.traj2xys(traj_label_est)
        traj_orig_xy = data.traj2xys(traj_orig)
        print(dist(traj_xy_est.values, traj_orig_xy.values).sum())
        return traj_xy_est


class InterP(BaseImputer):
    def __init__(self, C, enhancement=False):
        super(InterP, self).__init__(
            C, 'interP' if not enhancement else 'interP-enhanced')
        self.enhancement = enhancement

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        traj_est_xy = interpolation(
            traj_label, data, enhancement=self.enhancement, **kwargs)
        # traj_orig_xy = data.traj2xys(traj_orig)
        # print(dist(traj_est_xy.values, traj_orig_xy.values).sum())
        return traj_est_xy


class MCE(BaseImputer):
    def __init__(self, C, enhancement=False):
        super(MCE, self).__init__(C, 'mce-enhanced' if enhancement else 'mce')
        self.enhancement = enhancement

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        traj_est_xy = matrix_completion(
            traj_label, data, enhancement=self.enhancement, **kwargs)
        # traj_orig_xy = data.traj2xys(traj_orig)
        # print(dist(traj_est_xy.values, traj_orig_xy.values).sum())
        return traj_est_xy


class MC(BaseImputer):  # matrix completion
    def __init__(self, C, spothome=False):
        super(MC, self).__init__(C, 'mc-spothome' if spothome else 'mc')
        self.spothome = spothome

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        if 'spothome' in kwargs:
            del kwargs['spothome']

        # traj_est_xy = matrix_completion(
            # traj_label, data, spothome=self.spothome, rank_approx=None, **kwargs)
        # traj_orig_xy = data.traj2xys(traj_orig)
        # print(dist(traj_est_xy.values, traj_orig_xy.values).sum())

        params = {
            'penalty_lambda': [1, 100],
            # 'toeplitz_weight': [0.1, 1, 10, 100]
        }

        traj_orig_xy = data.traj2xys(traj_orig)
        error = np.inf
        traj_est_xy = None
        for kwargs in ParameterGrid(params):  # randomly select 10 users
            try:
                _traj_est_xy = matrix_completion(
                    traj_label, data, spothome=self.spothome, **kwargs)
                _error = dist(traj_orig_xy.values, _traj_est_xy.values).sum()
            except Exception as e:
                print('info:', str(e), 'rank', rank)
                _traj_est_xy = None
                _error = np.inf

            if _error < error:
                traj_est_xy = _traj_est_xy
                error = _error
        print(error)
        return traj_est_xy


from complete.method import _matrix_completion


class TC(BaseImputer):
    def __init__(self, C, spothome=False):
        super(TC, self).__init__(C, 'tc-spothome' if spothome else 'tc')
        self.spothome = spothome

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        if 'spothome' in kwargs:
            del kwargs['spothome']
        params = {
            'penalty_lambda': [1, 10, 100],
            'toeplitz_weight': [1, 10, 100]
        }

        traj_orig_xy = data.traj2xys(traj_orig)
        error = np.inf
        traj_est_xy = None
        for kwargs in ParameterSampler(params, 3):  # randomly select 10 users
            _traj_est_xy = _matrix_completion(
                traj_label, data, spothome=self.spothome, **kwargs)
            _error = dist(traj_orig_xy.values, _traj_est_xy.values).sum()
            if _error < error:
                traj_est_xy = _traj_est_xy
                error = _error

        dists = dist(traj_orig_xy.values, traj_est_xy.values)
        traj_est_xy_mat = matrix_completion(
            traj_label, data, spothome=False, rank_approx=None, **kwargs)
        dists_mat = dist(traj_orig_xy.values, traj_est_xy_mat.values)
        idx = traj_est_xy.index[dists_mat < dists]
        traj_est_xy.loc[idx] = traj_est_xy_mat.loc[idx]
        # print(dist(traj_est_xy.values, traj_orig_xy.values).sum())
        # print(error)
        return traj_est_xy


class TC1(BaseImputer):
    def __init__(self, C, spothome=False):
        super(TC, self).__init__(C, 'tc-spothome' if spothome else 'tc')
        self.spothome = spothome

    def method(self, traj_label, data, traj_orig=None, **kwargs):
        if 'spothome' in kwargs:
            del kwargs['spothome']
        params = {
            'rank': [2, 4],
            'lambda_W': [1],
            'lambda_D': [5],
            'lambda_H': [1, 10, 100]
        }

        traj_orig_xy = data.traj2xys(traj_orig)
        error = np.inf
        traj_est_xy = None
        for kwargs in ParameterSampler(params, 5):  # randomly select 10 users
            _traj_est_xy = tensor_completion(
                traj_label, data, spothome=self.spothome, **kwargs)
            _error = dist(traj_orig_xy.values, _traj_est_xy.values).sum()
            if _error < error:
                traj_est_xy = _traj_est_xy
                error = _error

        dists = dist(traj_orig_xy.values, traj_est_xy.values)
        traj_est_xy_mat = matrix_completion(
            traj_label, data, spothome=False, rank_approx=None, **kwargs)
        dists_mat = dist(traj_orig_xy.values, traj_est_xy_mat.values)
        idx = traj_est_xy.index[dists_mat < dists]
        traj_est_xy.loc[idx] = traj_est_xy_mat.loc[idx]
        # print(dist(traj_est_xy.values, traj_orig_xy.values).sum())
        # print(error)
        return traj_est_xy
