# ann
from functools import reduce, wraps
from math import gcd

import numpy as np
import pandas as pd
import scipy
import scipy.linalg
import scipy.linalg.interpolative

from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import RobustScaler, StandardScaler, MinMaxScaler

from complete.algorithm import KongMC #, HaLRTC, Inverse3dTC
from complete.algorithm import PmfMC, BiasMC
from complete.util import dist
from complete.data import data_dir

from sklearn.neighbors import NearestNeighbors


def toeplitz(vec, n):
    cols = [vec[i] if i < len(vec) else 0 for i in range(n)]
    rows = [vec[0] if i == 0 else 0 for i in range(n)]
    return scipy.linalg.toeplitz(rows, cols)


def min_most_freq(items):
    _count = items.value_counts()
    _count = _count[_count == _count.max()]
    return _count.index.min()


def max_most_freq(items):
    _count = items.value_counts()
    _count = _count[_count == _count.max()]
    return _count.index.max()


def home_boundray(begin_hours, end_hours):
    b1 = min_most_freq(begin_hours)
    e1 = max_most_freq(end_hours[end_hours > b1])

    e2 = max_most_freq(end_hours)
    b2 = min_most_freq(begin_hours[begin_hours <= e2])

    if (e1 - b1) > (e2 - b2):
        return b1, (e1 + 1)
    else:
        return b2, (e2 + 1)


def _spothome(trajectory_label):
    traj = trajectory_label
    try:
        # -12hours
        index = traj.index.copy() - pd.Timedelta(hours=12)
        # optimize
        # Filtering Calls in [22,7]
        b, e = 22, 7 + 24
        home_index = index[index.to_series().apply(
            lambda p: b - 12 <= p.hour < e - 12)]

        if len(home_index) == 0:
            return traj.copy()

        est_traj = traj.copy()
        est_traj.index = index

        home_traj = est_traj[home_index]
        if len(home_traj[home_traj.notnull()]) == 0:
            return traj.copy()

        counts = home_traj.value_counts(dropna=True)
        loc_home = counts.idxmax()
        if counts.max() / counts.sum() < 0.8:
            return traj.copy()

        home_locations = home_traj[home_traj == loc_home]
        # print(home_locations.groupby(home_locations.index.hour).count())
        # traj = trajectory_label.copy()
        # _traj = traj[(traj.index.hour >= 20) | (traj.index.hour < 9)]
        # _traj = _traj[_traj==loc_home]
        # print(_traj.groupby(_traj.index.hour).count())

        time_points = home_locations.index.to_series().groupby(home_locations.index.date).apply(
            lambda times: pd.DataFrame([{'begin_hour': times.min().hour, 'end_hour': times.max().hour}],
                                       index=[times.iloc[0].date()]))
        # print(time_points)
        # _time_points = time_points[time_points['begin_hour']
        #                            != time_points['end_hour']]
        # if len(_time_points.index) > 0:
        #     print(_time_points)
        #     time_points = _time_points

        home_begin_hour, home_end_hour = home_boundray(
            time_points['begin_hour'], time_points['end_hour'])

        # bug fix
        home_begin_hour = max(b - 12, home_begin_hour)
        home_end_hour = min(e - 12, home_end_hour)

        home_begin_hour = (home_begin_hour + 12) % 24
        home_end_hour = (home_end_hour + 12) % 24  # not included
        # print(home_begin_hour, home_end_hour)

        est_traj.index = traj.index.copy()

        # In this approach, if a user's location during tHu is not identified
        # and he was last seen at no more than 1 km from his home location,
        # he is moved to his home location.

        for date, locs in traj.groupby(traj.index.date):
            # check_home_period
            if home_begin_hour <= home_end_hour:
                selected = locs.index.to_series().apply(
                    lambda tp: home_begin_hour <= tp.hour < home_end_hour)
            elif home_begin_hour > home_end_hour:
                selected = (locs.index.hour >= home_begin_hour) | (
                    locs.index.hour < home_end_hour)
            else:
                raise EnvironmentError(
                    'Home hour error', home_begin_hour, home_end_hour)
            home_locs = locs[selected]
            ifFill = True
            for tp in home_locs.index:
                loc_id = home_locs.loc[tp]
                if not pd.isnull(loc_id) and loc_id != loc_home:
                    ifFill = False
            if ifFill:
                est_traj.loc[home_locs.index] = loc_home

        return est_traj

    except EnvironmentError as err:
        print("Error:", err, "happened in [Spothome] Trajectory:", traj)
        return traj.copy()


def interpolation(traj_label, data, enhancement=False, **kwargs):
    # S.Hoteit, S.Secci, S.Sobolevsky, C.Ratti, and G.Pujolle,
    # Estimating human trajectories and hotspots through mobile phone data.,
    # CN, vol. 64, pp. 296-307, 2014.
    # linear interpolation if Rg<32
    # else cubic interpolation
    # while we consider only urban users, so assuming they all have a Rg<32

    if enhancement:
        traj_label_orig = traj_label.copy()
        traj_label = _spothome1(traj_label_orig)

    traj_xy = data.traj2xys(traj_label)
    N = len(traj_xy.index)

    traj_est_xy = traj_xy.copy()
    i = 0
    while i < N:
        loc_i = traj_xy.iloc[i]  # lat,lon; x,y
        if pd.isnull(loc_i['x']):  # only one dimension is enough
            i += 1
        else:
            j = i + 1
            while j < N:
                loc_j = traj_xy.iloc[j]
                if pd.isnull(loc_j['x']):
                    j += 1
                elif j == i + 1:
                    break
                else:
                    # interpolate from (i,j+1)
                    t = [i, j]
                    x = [loc_i['x'], loc_j['x']]
                    y = [loc_i['y'], loc_j['y']]
                    f_x = scipy.interpolate.interp1d(t, x)
                    f_y = scipy.interpolate.interp1d(t, y)
                    for k in range(i + 1, j):
                        x_k = f_x(k)
                        y_k = f_y(k)
                        traj_est_xy.iloc[k] = pd.Series({'x': x_k, 'y': y_k})
                    break
            i = j

    # complete others
    traj_est_xy = traj_est_xy.fillna(
        method='ffill', axis=0).fillna(method='bfill', axis=0)

    if enhancement:
        points = data.traj2xys(traj_label).dropna().drop_duplicates().values
        neigh = NearestNeighbors(n_neighbors=1, metric=dist).fit(points)
        _, idx = neigh.kneighbors(X=traj_est_xy.values)
        traj_est_xy = pd.DataFrame(points[idx.reshape(-1), ], index=traj_est_xy.index,
                                   columns=traj_est_xy.columns)
    return traj_est_xy


def matrix_completion(traj_label, data,
                      spothome=False,
                      enhancement=False,
                      verbose=False,
                      rank_approx=None,
                      penalty_lambda=100,
                      toeplitz_weight=1,
                      **kwargs):
    if spothome or enhancement:
        traj_label = _spothome(traj_label)

    traj_xy = data.traj2xys(traj_label)

    X = traj_xy.values.copy()
    scaler = RobustScaler()

    rows = np.where(~traj_xy['x'].isnull())[0]
    points = X[rows, :].copy()
    X[rows, :] = scaler.fit_transform(X[rows, :])

    X = X.reshape(-1, 24 * 2)
    n_days = X.shape[0]
    X = np.vstack([X for _ in range(48 // n_days + 1)])
    rank = scipy.linalg.interpolative.estimate_rank(X, 0.1)
    # typically about 8 higher than the actual numerical rank
    rank = min([X.shape[0], X.shape[1], rank - 6])
    rank = max([2, rank])

    mask = np.ones(X.shape)
    mask[np.isnan(X)] = 0
    X[np.isnan(X)] = 0

    X_est = PmfMC(X, mask, k=rank, mu=penalty_lambda)
    # X_est = BiasMC(X, mask, k=rank, mu=penalty_lambda)

    X_est = X_est[range(n_days)]
    X_est = X_est.reshape(-1, 2)
    X_est = scaler.inverse_transform(X_est)
    X_est[rows, :] = points
    traj_est_xy = pd.DataFrame(
        X_est, index=traj_xy.index, columns=traj_xy.columns)
    # traj_est_xy = traj_est_xy.apply(
    #    lambda p: data.xy2towerxy(p['x'], p['y'], p.name), axis=1)

    if enhancement:
        points = data.traj2xys(traj_label).dropna().drop_duplicates().values
        neigh = NearestNeighbors(n_neighbors=1, metric=dist).fit(points)
        _, idx = neigh.kneighbors(X=traj_est_xy.values)
        traj_est_xy = pd.DataFrame(points[idx.reshape(-1), ], index=traj_est_xy.index,
                                   columns=traj_est_xy.columns)

    return traj_est_xy


def _matrix_completion(traj_label, data,
                       spothome=False,
                       verbose=False,
                       rank_approx=None,
                       penalty_lambda=1,
                       toeplitz_weight=1,
                       **kwargs):
    if spothome:
        traj_label = _spothome(traj_label)

    traj_xy = data.traj2xys(traj_label)

    X = traj_xy.values.copy()
    scaler = RobustScaler()
    # scaler = StandardScaler()

    rows = np.where(~traj_xy['x'].isnull())[0]
    points = X[rows, :].copy()
    X[rows, :] = scaler.fit_transform(X[rows, :])

    X = X.reshape(-1, 24 * 2)
    n_days = X.shape[0]
    X = np.vstack([X for _ in range(48 // n_days + 1)])
    omega = ~np.isnan(X)
    X[~omega] = 0.

    X_est, _ = KongMC(X, omega,
                      init='svd',
                      rank_approx=rank_approx,
                      penalty_lambda=penalty_lambda,
                      toeplitz_weight=toeplitz_weight,
                      verbose=verbose)
    X_est = X_est[range(n_days)]
    X_est = X_est.reshape(-1, 2)
    X_est = scaler.inverse_transform(X_est)
    X_est[rows, :] = points
    traj_est_xy = pd.DataFrame(
        X_est, index=traj_xy.index, columns=traj_xy.columns)
    traj_est_xy = traj_est_xy.apply(
        lambda p: data.xy2towerxy(p['x'], p['y'], p.name), axis=1)
    return traj_est_xy


# best_params = {'rank':8, lambda_A': 0.01, 'lambda_B': 10.0, 'lambda_C': 0.0,
#                'lambda_W': 1.0, 'lambda_D': 0.1, 'lambda_H': 0.01}
# best_params = {'rank':6, lambda_A': 0.1, 'lambda_B': 0.1, 'lambda_C': 0.01,
#                'lambda_W': 0.1, 'lambda_D': 0.1, 'lambda_H': 0.01}

def _dummyhome(traj_label):
    def _time_to_col(time):
        return '%.3d' % (time.weekday() * 100 + time.hour)

    dummy_index = traj_label.index.to_series().apply(_time_to_col).values
    hook = {}
    traj_est = traj_label.copy()
    for time, loc in traj_label[traj_label.notnull()].iteritems():
        col = _time_to_col(time)
        if col not in hook:
            hook[col] = 1
            # traj_est[(dummy_index == col) & (traj_est.isnull())] = loc
            _index = traj_est[(dummy_index == col)
                              ].index.to_series().sample(frac=0.4).values
            traj_est.loc[_index] = loc
    return traj_est


_spothome1 = _spothome


def _spothome(traj_label):
    traj_est = traj_label.copy()
    for _, _traj in traj_label.groupby(traj_label.index.weekday):
        _traj = _spothome1(_traj)
        traj_est.loc[_traj.index] = _traj.values
    return traj_est
# _spothome = _dummyhome


def tensor_completion(traj_label, data,
                      spothome=True,
                      rank=4,
                      lambda_A=0,
                      lambda_B=0,
                      lambda_C=0,
                      lambda_W=10.,
                      lambda_D=5.,
                      lambda_H=5.,
                      verbose=False,
                      init='random',
                      **kwargs):

    from complete.algorithm import MyTC
    loss_rate = traj_label[traj_label.isnull()].size / traj_label.size

    if spothome:
        traj_label_orig = traj_label.copy()
        traj_label = _spothome(traj_label_orig)

    traj_xy = data.traj2xys(traj_label)
    scaler = RobustScaler(with_centering=True)
    #scaler = StandardScaler()
    rows = np.where(~traj_xy['x'].isnull())[0]
    points = traj_xy.values[rows, :].copy()

    X = traj_xy.values.copy()
    X[rows, :] = scaler.fit_transform(X[rows, :])
    n_hrs = X.shape[0]
    window = 5  # 5 days only weekdays
    X = X.reshape(-1, window, 24 * 2)
    omega = ~np.isnan(X)
    X[~omega] = 0.
    X_init = None

    matW = toeplitz([1, -1, 0], X.shape[0])
    matD = toeplitz([1, -1, 0], X.shape[1])
    matH = toeplitz([1, 0, -2, 0, 1], X.shape[2])

    X_est, _, _ = MyTC(X, omega,
                       X=X_init,
                       rank=rank,
                       l2_params=[lambda_A, lambda_B, lambda_C],
                       regu_params=[lambda_W, lambda_D, lambda_H],
                       regu_matrices=[matW, matD, matH],
                       init=init,
                       maxIter=50,
                       non_negative=False,
                       verbose=verbose)

    X_est = X_est.reshape(-1, 2)
    X_est = scaler.inverse_transform(X_est)
    X_est[rows, :] = points
    traj_est_xy = pd.DataFrame(
        X_est, index=traj_xy.index, columns=traj_xy.columns)
    traj_est_xy = traj_est_xy.apply(
        lambda p: data.xy2towerxy(p['x'], p['y'], p.name), axis=1)
    return traj_est_xy
