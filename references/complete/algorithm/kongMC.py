import numpy as np
import scipy.linalg
import scipy.linalg.interpolative


def ev(X):
    y = np.sqrt((X ** 2).sum())
    # return np.linalg.norm(X, 'fro')
    return y


def toeplitz(vec, n):
    cols = [vec[i] if i < len(vec) else 0 for i in range(n)]
    rows = [vec[0] if i == 0 else 0 for i in range(n)]
    return scipy.linalg.toeplitz(rows, cols)


# function Y = myInverse(B,L,lambda,S)
# 	[n,t]=size(B);
# 	r=size(L,2);
# 	Y=zeros(r,t);
# 	Ir=diag(ones(r,1));
# 	Zr=zeros(r,1);
# 	for i=1:t
# 		Bi=diag(B(:,i));
# 		P=[Bi*L; sqrt(lambda)*Ir];
# 		Q=[S(:,i); Zr];
# 		Y(:,i)=(P'*P)\(P'*Q);
#     end


def FastInverse_slow_ver(B, L, penalty_lambda, S):
    n, t = B.shape
    r = L.shape[1]
    Y = np.zeros((r, t))
    Zr = np.zeros(r)
    Ir = np.diag(np.sqrt(penalty_lambda) * np.ones(r))

    for i in range(t):
        BiL = np.einsum('i,ij->ij', B[:, i], L)  # np.diag(B[:, i]).dot(L)
        P = np.concatenate((BiL, Ir))
        # np.hstack((S[:, i], Zr)).reshape(-1, 1)
        Q = np.concatenate((S[:, i], Zr))[:, np.newaxis]
        Y[:, i] = scipy.linalg.solve(P.conj().T.dot(P), P.conj().T.dot(Q),
                                     sym_pos=True, overwrite_a=True, overwrite_b=True, check_finite=False).ravel()
    return Y


# @jit(nopython=True, nogil=True, cache=True)
def FastInverse(B, L, penalty_lambda, S):
    n, t = B.shape
    r = L.shape[1]
    Y = np.zeros((r, t))
    Zr = np.zeros(r)
    Ir = np.diag(np.sqrt(penalty_lambda) * np.ones(r))

    for i in range(t):
        # np.einsum('i,ij->ij',B[:,i],L) #np.diag(B[:, i]).dot(L)
        if B[:, i].sum() > 0:
            BiL = B[:, i].copy().reshape(-1, 1) * L
            P = np.concatenate((BiL, Ir))
            # np.concatenate((S[:,i],Zr))[:,np.newaxis] #np.hstack((S[:, i],
            # Zr)).reshape(-1, 1)
            Q = np.concatenate((S[:, i], Zr)).reshape(-1, 1)
            # Y[:, i] = scipy.linalg.solve(P.conj().T.dot(P), P.conj().T.dot(Q),
            # sym_pos=True, overwrite_a=True, overwrite_b=True,
            # check_finite=False).ravel()
            Y[:, i] = np.linalg.solve(np.dot(P.T, P), np.dot(P.T, Q)).ravel()
    return Y


# function [L,R,v]=estics(S,B,r,H,T,lambda,MaxIter)
# 	if nargin < 6, lambda = 1e-2;     end
# 	if nargin < 7, MaxIter = 20;      end

# 	[n,t]=size(S);

# 	v=1e50;
# 	Lopt=ones(n,r);

# 	Il=diag(ones(r,1));
# 	Ir=diag(ones(t,1));
# 	Zt=zeros(t);
# 	Zn=zeros(n);
# 	Ot=ones(t);
# 	On=ones(n);
# 	for i=1:MaxIter
# 		Ropt=myInverse(B,Lopt,lambda,S)';
# 		Lopt=myInverse(B',Ropt,lambda,S')';
# 		%Ropt=myInverseFeature(B,Lopt,lambda,S,H)';
# 		%Lopt=myInverseFeature(B',Ropt,lambda,S',T')';
# 		X=Lopt*Ropt';
#         vopt1=ev(B.*X-S);
#         vopt2=lambda*(ev(Lopt)+ev(Ropt'));
#         vopt3=ev(H*X);
#         vopt4=ev(X*T);
#         vopt=vopt1+vopt2+vopt3+vopt4;
# 		%vopt=ev(B.*X-S)+lambda*(ev(Lopt)+ev(Ropt'))+ev(H*X)+ev(X*T);
#         %disp([vopt1 vopt2 vopt3 vopt4]);
# 		if vopt<v
# 			v=vopt;
# 			L=Lopt;
# 			R=Ropt;
# 		end
#     end

def KongMC(S, Omega,
           rank_approx=None,
           init='svd',
           penalty_lambda=1e-6,
           toeplitz_weight=1e-1,
           maxIter=20,
           X=None,
           verbose=False):
    M = S.copy()
    M[~Omega] = 0.

    if X is None:
        X = M.copy()
        X[~Omega] = M[Omega].mean()

    n, t = M.shape
    T = toeplitz([1, 0, -2, 0, 1], t).T
    T = T.dot(T)

    # T = scipy.linalg.toeplitz([1.] + np.zeros(t - 1).tolist(),
    #                           [1, -1] + np.zeros(t - 2).tolist())
    # T[0, 0] = 0

    if rank_approx is None:
        r = scipy.linalg.interpolative.estimate_rank(S, 0.05)
    elif rank_approx <= 1:
        r = int(n * rank_approx)
    else:
        r = rank_approx

    errList = np.zeros((maxIter, 1))

    if init == 'ones':
        Lopt = np.ones((n, r))
        Ropt = np.ones((t, r))
    elif init == 'svd':
        idx, proj = scipy.linalg.interpolative.interp_decomp(X, r)
        Lopt = X[:, idx[:r]].copy()
        Ropt = np.hstack([np.eye(r), proj])[:, np.argsort(idx)].T
    else:
        Lopt = np.random.rand(n, r)
        Ropt = np.random.rand(t, r)

    L, R = Lopt, Ropt

    B = np.ones(Omega.shape)
    B[~Omega] = 0.

    # normT = np.linalg.norm(M.ravel())
    normT = np.linalg.norm(M[Omega].ravel())
    if normT == 0.:  # all values in S is the same, completion is meaningless
        return X, []

    # estics
    X = Lopt.dot(Ropt.conj().T)
    v1 = ev(X[Omega] - M[Omega])
    v2 = penalty_lambda * (ev(Lopt) + ev(Ropt))
    v3 = toeplitz_weight * ev(np.dot(X, T))
    v = v1 + v2 + v3
    #v = np.inf
    n_iter = 0
    for k in range(maxIter):
        n_iter = k
        if verbose and ((k + 1) % 5 == 0):
            print('KongMC: iterations = %d, difference=%f' %
                  (k + 1, errList[k - 1]))

        #Ropt = FastInverse(B, Lopt, penalty_lambda, M).conj().T
        Lopt = FastInverse(B.conj().T, Ropt, penalty_lambda,
                           M.conj().T).conj().T
        Ropt = FastInverse(B, Lopt, penalty_lambda, M).conj().T

        lastX = X.copy()
        X = Lopt.dot(Ropt.conj().T)

        vopt1 = ev(X[Omega] - M[Omega])
        vopt2 = penalty_lambda * (ev(Lopt) + ev(Ropt))
        vopt3 = toeplitz_weight * ev(np.dot(X, T))
        vopt = vopt1 + vopt2 + vopt3
        if vopt < v:
            L = Lopt
            R = Ropt
            v = vopt
        # compute the error
        errList[k] = scipy.linalg.norm((X - lastX)[Omega].ravel()) / normT
        if errList[k] < 1e-8:
            break

    X = L.dot(R.conj().T)
    X[Omega] = M[Omega]

    errList = errList[0:n_iter + 1]
    if verbose:
        print('FaLRTC ends: total iterations = %d, difference=%f' %
              (n_iter + 1, errList[n_iter]))

    return X, errList
