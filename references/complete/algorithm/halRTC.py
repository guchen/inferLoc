import numpy as np
import scipy


def shiftdim(X, n):
    T = X
    for _ in range(n):
        T = np.moveaxis(T, 0, -1)
    return T


def Fold(X, dim, axis):
    # it seems np.roll and circshift is reverse
    dim = np.roll(dim, [axis, axis])
    X = shiftdim(X.reshape(dim, order='F').copy(), len(dim) - axis)
    return X


def Unfold(X, axis):
    dim = X.shape
    T = shiftdim(X, axis)
    return T.reshape(dim[axis], -1, order='F').copy()


def Pro2TraceNorm(Z, tau):
    m, n = Z.shape
    if m * 2 < n:
        AAT = Z.dot(Z.conj().T)
        S, sigma2, Dh = scipy.linalg.svd(AAT, lapack_driver='gesvd')
        D = Dh.T
        # sigma2 = np.diag(sigma2)
        V = np.sqrt(sigma2)
        # np.spacing is eps in matlab
        tol = np.max(Z.shape) * np.spacing(V.max(0))
        n = (V > np.maximum(tol, tau)).astype(int).sum()
        mid = np.maximum(V[0:n] - tau, 0) / V[0:n]
        X = S[:, 0:n].dot(np.diag(mid)).dot(S[:, 0:n].conj().T).dot(Z)
        return X, n, sigma2

    if m > 2 * n:
        X, n, sigma2 = Pro2TraceNorm(Z.conj().T, tau)
        X = X.conj().T
        return X, n, sigma2

    # U, S, Vh = linalg.svd(a), V = Vh.T
    S, V, Dh = scipy.linalg.svd(Z, lapack_driver='gesvd')
    D = Dh.T

    sigma2 = V ** 2
    n = (V > tau).astype(int).sum()
    X = S[:, 0:n].dot(np.maximum(np.diag(V)[0:n, 0:n] -
                                 tau, 0)).dot(D[:, 0:n].conj().T)

    return X, n, sigma2


def HaLRTC(S, Omega, alpha, beta=1e-6, maxIter=500, epsilon=1e-5, X=None, verbose=False):
    T = S.copy()
    T[~Omega] = 0.
    if X is None:
        X = T.copy()
        X[~Omega] = T[Omega].mean()
        # X[~Omega] = 0.

    errList = np.zeros((maxIter, 1))
    dim = T.shape
    ndim = len(T.shape)
    # ndim = T.shape[-1]
    normT = np.linalg.norm(T[Omega].ravel())
    if normT == 0.:  # all values in S is the same, completion is meaningless
        return X, []

    Y = {i: None for i in range(ndim)}
    M = {i: np.zeros(ndim) for i in range(ndim)}
    Msum = np.zeros(dim)
    Ysum = np.zeros(dim)

    n_iter = 0
    for k in range(maxIter):
        n_iter = k
        if verbose and ((k + 1) % 20 == 0):
            print('HaLRTC: iterations = %d, difference=%f' %
                  (k + 1, errList[k - 1]))

        beta *= 1.05

        # update Y
        Msum *= 0
        Ysum *= 0

        for i in range(ndim):
            trace, _, _ = Pro2TraceNorm(
                Unfold(X - M[i] / beta, i), alpha[i] / beta)
            Y[i] = Fold(trace, dim, i)
            Msum += M[i]
            Ysum += Y[i]

        # update X
        lastX = X.copy()
        X = (Msum + beta * Ysum) / (ndim * beta)
        X[Omega] = T[Omega]

        # update M
        for i in range(ndim):
            M[i] = M[i] + beta * (Y[i] - X)

        # compute the error
        errList[k] = np.linalg.norm(X.ravel() - lastX.ravel()) / normT
        if errList[k] < epsilon:
            break

    errList = errList[0:n_iter + 1]
    if verbose:
        print('HaLRTC ends: total iterations = %d, difference=%f' %
              (n_iter + 1, errList[n_iter]))

    return X, errList
