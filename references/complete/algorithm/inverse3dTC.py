import numpy as np
import scipy
import scipy.linalg


def ev(X):
    y = np.sqrt((X ** 2).sum())
    # return np.linalg.norm(X, 'fro')
    return y


def shiftdim(X, n):
    T = X
    for _ in range(n):
        T = np.moveaxis(T, 0, -1)
    return T


def fold(X, dim, axis):
    dim = np.roll(dim, [axis, axis])  # it seems np.roll and circshift is reverse
    X = shiftdim(X.reshape(dim, order='F').copy(), len(dim) - axis)
    return X


def unfold(X, axis):
    dim = X.shape
    T = shiftdim(X, axis)
    return T.reshape(dim[axis], -1, order='F').copy()


def partial_svd(matrix, n_eigenvecs=None):
    """Computes a fast partial SVD on `matrix`
        if `n_eigenvecs` is specified, sparse eigendecomposition
        is used on either matrix.dot(matrix.T) or matrix.T.dot(matrix)
    Parameters
    ----------
    matrix : 2D-array
    n_eigenvecs : int, optional, default is None
        if specified, number of eigen[vectors-values] to return
    Returns
    -------
    U : 2D-array
        of shape (matrix.shape[0], n_eigenvecs)
        contains the right singular vectors
    S : 1D-array
        of shape (n_eigenvecs, )
        contains the singular values of `matrix`
    V : 2D-array
        of shape (n_eigenvecs, matrix.shape[1])
        contains the left singular vectors
    """
    # Check that matrix is... a matrix!
    if matrix.ndim != 2:
        raise ValueError('matrix be a matrix. matrix.ndim is {} != 2'.format(
            matrix.ndim))

    # Choose what to do depending on the params
    dim_1, dim_2 = matrix.shape
    if dim_1 <= dim_2:
        min_dim = dim_1
    else:
        min_dim = dim_2

    if n_eigenvecs is None or n_eigenvecs >= min_dim:
        # Default on standard SVD
        U, S, V = scipy.linalg.svd(matrix)
        U, S, V = U[:, :n_eigenvecs], S[:n_eigenvecs], V[:n_eigenvecs, :]
        return U, S, V
    else:
        # We can perform a partial SVD
        # First choose whether to use X * X.T or X.T *X
        if dim_1 < dim_2:
            S, U = scipy.sparse.linalg.eigsh(np.dot(matrix, matrix.T), k=n_eigenvecs, which='LM')
            S = np.sqrt(S)
            V = np.dot(matrix.T, U * 1 / S[None, :])
        else:
            S, V = scipy.sparse.linalg.eigsh(np.dot(matrix.T, matrix), k=n_eigenvecs, which='LM')
            S = np.sqrt(S)
            U = np.dot(matrix, V) * 1 / S[None, :]

        # WARNING: here, V is still the transpose of what it should be
        U, S, V = U[:, ::-1], S[::-1], V[:, ::-1]
        return U, S, V.T


def factors_to_tensor_slow(factors):
    rank = factors[0].shape[1]
    shape = [factor.shape[0] for factor in factors]

    tensor = np.zeros(tuple(shape))
    for idx, _ in np.ndenumerate(tensor):
        v = 0.
        for r in range(rank):
            vr = 1.
            for k in range(len(idx)):
                vr *= factors[k][idx[k], r]
            v += vr
        tensor[idx] = v
    return tensor


def factors_to_tensor(factors):
    n_dim = len(factors)
    arguments = []
    for axis in range(n_dim):
        arguments.append(factors[axis])
        arguments.append([axis + 1, 0])
    arguments.append([axis + 1 for axis in range(n_dim)])
    return np.einsum(*arguments)


def khatri_rao(matrices, reverse=False):
    # Compute the Khatri-Rao product of all matrices in list "matrices".
    # If reverse is true, does the product in reverse order.
    matorder = range(len(matrices)) if not reverse else list(reversed(range(len(matrices))))

    # Error checking on matrices; compute number of rows in result.
    # N = number of columns (must be same for each input)
    N = matrices[0].shape[1]
    # Compute number of rows in resulting matrix
    # After the loop, M = number of rows in result.
    M = 1
    for i in matorder:
        if matrices[i].ndim != 2:
            raise ValueError("Each argument must be a matrix.")
        if N != (matrices[i].shape)[1]:
            raise ValueError("All matrices must have the same number of columns.")
        M *= (matrices[i].shape)[0]

    # Computation
    # Preallocate result.
    P = np.zeros((M, N))

    # n loops over all column indices
    for n in range(N):
        # ab = nth col of first matrix to consider
        ab = matrices[matorder[0]][:, n]
        # loop through matrices
        for i in matorder[1:]:
            # Compute outer product of nth columns
            ab = np.outer(matrices[i][:, n], ab[:])
        # Fill nth column of P with flattened result
        P[:, n] = ab.flatten()
    return P


def FastInverse(B, L, penalty_lambda, S):
    n, t = B.shape
    r = L.shape[1]
    Y = np.zeros((r, t))
    Zr = np.zeros(r)
    Ir = np.diag(np.sqrt(penalty_lambda) * np.ones(r))

    for i in range(t):
        if B[:,i].sum()>0:
            BiL = B[:, i].copy().reshape(-1, 1) * L  # np.einsum('i,ij->ij',B[:,i],L) #np.diag(B[:, i]).dot(L)
            P = np.concatenate((BiL, Ir))
            Q = np.concatenate((S[:, i], Zr)).reshape(-1,
                                                      1)  # np.concatenate((S[:,i],Zr))[:,np.newaxis] #np.hstack((S[:, i], Zr)).reshape(-1, 1)
            # Y[:, i] = scipy.linalg.solve(P.conj().T.dot(P), P.conj().T.dot(Q),
            # sym_pos=True, overwrite_a=True, overwrite_b=True, check_finite=False).ravel()
            Y[:, i] = np.linalg.solve(np.dot(P.T, P), np.dot(P.T, Q)).ravel()
    return Y


def Inverse3dTC(S, Omega, rank_approx, penalty_lambda, maxIter=200, init='random', epsilon=1e-5,
                X=None,
                verbose=False):
    T = S.copy()
    T[~Omega] = 0.
    if len(T.shape) != 3 or Omega.shape != T.shape:
        raise EnvironmentError('')

    if X is None:
        X = T.copy()
        X[~Omega] = T[Omega].mean()

    if 0 < rank_approx < 1:
        rank = max(int(min(X.shape) * rank_approx),1)
    elif rank_approx >= 1:
        rank = rank_approx
    else:
        raise EnvironmentError('rank_approach must > 0.')

    n_dim = len(X.shape)
    if init =='random':
        factors = [np.random.RandomState(19881102).rand(X.shape[i], rank) for i in range(n_dim)]
    elif init =='svd':
        if rank >= max(X.shape):
            raise EnvironmentError('for svd init, rank must smaller than max(T)')
        factors = []
        for axis in range(n_dim):
            U, _, _ = partial_svd(unfold(X, axis), n_eigenvecs=rank)

            if X.shape[axis] < rank:
                # TODO: this is a hack but it seems to do the job for now
                factor = np.zeros((U.shape[0], rank))
                factor[:, X.shape[axis]:] = np.random.rand(U.shape[0], rank - X.shape[axis])
                factor[:, :X.shape[axis]] = U
                U = factor
            factors.append(U[:, :rank])
    else:
        raise EnvironmentError('init must be "init" or "svd".')

    errList = np.zeros((maxIter, 1))
    normT = np.linalg.norm(X[Omega].ravel())
    if normT == 0.:  # all values in S is the same, completion is meaningless
        return X, []

    B = np.ones(Omega.shape)
    B[~Omega] = 0.

    t = X.shape[1]
    H = scipy.linalg.toeplitz([1.] + np.zeros(t - 1).tolist(),
                              [1, -1] + np.zeros(t - 2).tolist())

    n_iter = 0

    P, Q, R = tuple(factors)
    Popt, Qopt, Ropt = None, None, None
    vopt,vlast = np.inf, np.inf
    for k in range(maxIter):
        n_iter = k
        if verbose and ((k + 1) % 5 == 0):
            print('Inverse3dTC: iterations = %d, difference=%f' % (k + 1, errList[k - 1]))

        R = FastInverse(unfold(B, 2).T, khatri_rao([P, Q]), penalty_lambda, unfold(X, 2).T).T
        Q = FastInverse(unfold(B, 1).T, khatri_rao([R, P]), penalty_lambda, unfold(X, 1).T).T
        P = FastInverse(unfold(B, 0).T, khatri_rao([Q, R]), penalty_lambda, unfold(X, 0).T).T

        X = factors_to_tensor([P, Q, R])
        if k==0:
            H_weight = penalty_lambda * ev(B * X) / ev(H.dot(unfold(B*X, 1)))

        # compute the error
        v0 = ev(B * (X - T))
        v1 = penalty_lambda * (ev(P) + ev(Q) + ev(R))
        v2 = H_weight * ev(H.dot(unfold(X, 1)))
        v = v0+v1+v2
        errList[k] = v
        if v < vopt:
            vopt = v
            Popt, Qopt, Ropt = P, Q, R

        if v >= vlast:
            break
        vlast = v


    errList = errList[0:n_iter + 1]
    if verbose:
        print('FaLRTC ends: total iterations = %d, difference=%f' % (n_iter + 1, errList[n_iter]))

    X = factors_to_tensor([Popt, Qopt, Ropt])
    X[Omega] = T[Omega]
    return X, errList
