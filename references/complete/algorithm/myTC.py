# tensor factorization approach mentioned in the paper
import numpy as np
import scipy.linalg
import torch
from torch import from_numpy

import tensorly as tl
tl.set_backend('pytorch')
import tensorly.random
import tensorly.decomposition
from tensorly.tenalg import kronecker, khatri_rao, mode_dot
from tensorly import unfold, kruskal_to_tensor as cpd
from tensorly.backend.pytorch_backend import tensor
from copy import deepcopy
from functools import reduce


def ev(X):
    return torch.sum(X**2)


def vec(X):
    # vector combining columns
    # slow version
    _X_numpy = X.numpy().reshape(-1, order='F')
    return from_numpy(_X_numpy)


def identity(n):
    return tensor(np.identity(n))


def MyTC(T, Omega, rank, l2_params, regu_params, regu_matrices, X=None, non_negative=False, init='random', maxIter=100, eta=0.1, verbose=False):
    n_dim = len(T.shape)

    if not (n_dim > 2):
        raise EnvironmentError(f'Need a tensor but T.shape={T.shape}')
    if T.shape != Omega.shape:
        raise EnvironmentError(
            f'Need T and Omega have same shape but T.shape=', T.shape, 'Omega.shape=', Omega.shape)
    if not (rank >= 2):
        raise EnvironmentError(f'Need rank>=2 but rank={rank}')
    if len(l2_params) != n_dim:
        raise EnvironmentError('T.shape=', T.shape,
                               'but l2_params=', l2_params)
    if len(regu_params) != n_dim:
        raise EnvironmentError('T.shape=', T.shape,
                               'but regu_params=', regu_params)
    if len(regu_matrices) != n_dim:
        raise EnvironmentError('T.shape=', T.shape,
                               'but len(regu_matrices)=', len(regu_matrices))
    for i in range(n_dim):
        if regu_matrices[i].shape != (T.shape[i], T.shape[i]):
            raise EnvironmentError(f'Need regu_matrices[{i}].shape=({T.shape[i]},{T.shape[i]})',
                                   'but regu_matrices[{i}].shape=', regu_matrices[i].shape)
    if not (maxIter > 1):
        raise EnvironmentError('Need maxIter>1 but maxIter=', maxIter)

    _T = tensor(T)
    if X is None:
        X = T.copy()
        X[~Omega] = T[Omega].mean()
    else:
        assert X.shape == T.shape

    B = np.ones(Omega.shape)
    B[~Omega] = 0.
    X, B = tensor(X), tensor(B)

    BX = B * X
    unfolded_B_vec = [vec(unfold(B, i)).view(-1, 1) for i in range(n_dim)]
    unfolded_BX_vec = [vec(unfold(BX, i)) for i in range(n_dim)]

    # to pytorch
    regu_matrices = [tensor(_item) for _item in regu_matrices]

    # lamda_I
    l2_identity_matrics = [
        np.sqrt(l2_params[i]) * identity(T.shape[i] * rank) for i in range(n_dim)]
    identity_matrices = [identity(T.shape[i]) for i in range(n_dim)]
    # CPD
    if verbose:
        print(f'Initialize method: {init}')
    if init == 'random':
        factors = tl.random.cp_tensor(X.size(), rank)
    elif init == 'one':
        factors = [0.9 * torch.ones((T.shape[i], rank)) for i in range(n_dim)]
    elif non_negative:
        factors = tl.decomposition.non_negative_parafac(
            X, rank, n_iter_max=10, verbose=verbose)
    else:
        factors = tl.decomposition.parafac(
            X, rank, n_iter_max=10, verbose=verbose)

    final_factors = deepcopy(factors)
    losses = []
    last_loss = np.inf
    stop_mark = 0
    for k in range(maxIter):
        # update vectors
        for i in range(n_dim):
            D = unfolded_B_vec[i]  # vec(unfold(B,i)).reshape(-1,1)
            KR = khatri_rao(factors, reverse=True, skip_matrix=i)
            M = [D * kronecker([KR, identity(T.shape[i])])]  # item0
            if l2_params[i] > 0:
                M.append(l2_identity_matrics[i])
                # M.append(np.sqrt(l2_params[i])*identity(T.shape[i]*rank))
            if regu_params[i] > 0:
                M.append(np.sqrt(regu_params[i]) *
                         kronecker([KR, regu_matrices[i]]))
            _M = []
            for j in range(n_dim):
                if j != i and regu_params[j] > 0:
                    _factor_j = factors[j]
                    factors[j] = regu_matrices[j].mm(
                        _factor_j)  # numpy.dot == torch.mm
                    _KR = khatri_rao(factors, reverse=True, skip_matrix=i)
                    _M.append(
                        np.sqrt(regu_params[j]) * kronecker([_KR, identity_matrices[i]]))
                    factors[j] = _factor_j
            M += _M
            M = torch.cat(M, 0)
            N = torch.zeros(M.shape[0],)
            N[0:np.prod(T.shape)] = unfolded_BX_vec[i]  # vec(unfold(B*X,i))
            try:
                if non_negative:
                    vec_factor_i = scipy.optimize.nnls(M.numpy(), N.numpy())[0]
                else:
                    vec_factor_i = scipy.linalg.lstsq(
                        M.numpy(), N.numpy())[0]
                    # vec_factor_i = torch.gels(N,M)[0]
                    # vec_factor_i = vec_factor_i[0:T.shape[i] * rank].numpy()
                factors[i] = tensor(vec_factor_i.reshape(-1, rank, order='F'))
            except:
                break

        # compute loss
        X = cpd(factors)
        fit_loss = ev(B * (X - _T))  # item0
        l2_loss = np.sum([l2_params[i] * ev(factors[i]) if l2_params[i] != 0 else 0
                          for i in range(n_dim)])  # item1
        regu_loss = np.sum([regu_params[i] * ev(mode_dot(X, regu_matrices[i], mode=i)) if regu_params[i] != 0 else 0.
                            for i in range(n_dim)])  # item2
        loss = fit_loss + l2_loss + regu_loss
        if verbose:
            print(
                f'Iteration {k}, L={loss}, |B*(X-T)|^2={fit_loss}, L1={fit_loss+regu_loss}')
        # log
        losses.append(loss)
        # convergence
        if loss < last_loss:
            stop_mark = 0
            final_factors = deepcopy(factors)
        else:
            stop_mark += 1

        if k > 20 and (stop_mark > 2 or np.abs(loss - last_loss) < eta):
            if verbose:
                print(f'Ended iteration={k}')
            break
        else:
            last_loss = loss

    # to numpy
    X = cpd(final_factors).numpy()
    X[Omega] = T[Omega]
    return X, losses, final_factors
