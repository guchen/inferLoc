import gzip
import os
import pickle
from functools import partial
from multiprocessing import Pool

import numpy as np
import pandas as pd
from tqdm import tqdm, tqdm_notebook

base_dir = os.path.dirname(os.path.realpath(__file__))

DataSourceList = {
    'VL1H2k100d': base_dir + '/../data/VL2k100d/locs_vols_1H_selected_completed',
    'VL1H7k150d': base_dir + '/../data/VL7k150d/locs_vols_1H_selected_completed',
    'VL1H7k150d-Orig': base_dir + '/../data/VL7k150d/locs_vols_1H_selected',
    'V15M7k150d': base_dir + '/../data/VL7k150d/vols_15M_selected',
    'V15M100k150d': base_dir + '/../data/V100k150d/vols_15M_150d_10%',
    'V1H100k150d': base_dir + '/../data/V100k150d/vols_1H_150d_10%'
}


def _merge_vols(Grouper):
    return Grouper.sum().dropna()
    # return vols.values.sum()


def _merge_locations(Grouper):
    def _merge(locations):
        if len(locations.index) == 0:
            return 'DROP'
        else:
            good_locations = locations[(
                                           locations != '') & (locations.notnull())]
            if len(good_locations.index) == 0:
                return '?'
            else:
                return good_locations[0]

    locs = Grouper.apply(_merge)
    return locs[locs != 'DROP']


def _datetimes_to_periods(index):
    def _sep(hour):
        if hour < 8:
            return 4
        elif 8 <= hour < 18:
            return 12
        elif hour >= 18:
            return 22

    return index.map(
        lambda dt: dt.replace(hour=_sep(dt.hour)))


def _resample_ts(ts, F, merge_func):
    if F == '15M':
        # groups = ts.groupby(ts.index.map(lambda dt: dt.replace(
        # second=0, minute=dt.minute // 15 * 15)))
        groups = ts.resample('15T')
    elif F == '1D':
        # groups = ts.groupby(ts.index.date)
        groups = ts.resample('1D')
    elif F == '1H':
        # groups = ts.groupby(ts.index.map(lambda dt: dt.replace(
        # second=0, minute=0)))
        groups = ts.resample('60T')
    elif F == 'P':
        groups = ts.groupby(_datetimes_to_periods(ts.index))
    else:
        raise EnvironmentError('F must be in', ['15M', '1D', '1H', 'P'])

    return merge_func(groups)


def vols_to_mags(ts):
    return np.floor(np.log10(ts + 0.1)) + 1


def vols_to_log10s(ts):
    return np.log10(ts + 0.1)


def mags_to_log10s(mags):
    return mags - 1


def mags_to_vols(mags):
    log10s = mags_to_log10s(mags)
    values = np.floor(np.power(10, log10s) - 0.1)
    values[values < 1] == 0.
    return values


def log10s_to_vols(log10s):
    values = np.floor(np.power(10, log10s) - 0.1)
    values[values < 1] == 0.
    return values


def log10s_to_mags(log10s):
    return np.floor(log10s) + 1


def _parallel_compute_func_wrapper(args, compute_func, mode='v'):
    dt = pd.DataFrame([])
    if mode == 'v':
        phone, ts_v, kwargs = args
        compute_func(phone, ts_v, dt, **kwargs)
    elif mode == 'vl':
        phone, ts_v, ts_l, kwargs = args
        compute_func(phone, ts_v, ts_l, dt, **kwargs)
    else:
        raise ValueError('mode is v or vl')

    return dt


class DataSource():
    def __init__(self, name, jupyter_notebook=True, since=0, prefix=''):
        if name not in DataSourceList:
            raise EnvironmentError('name must be in', DataSourceList.keys())

        self.name = name
        self.tqdm = tqdm_notebook if jupyter_notebook else tqdm
        self.data_path = DataSourceList[name]
        self.since = since
        self.prefix = prefix

        # load data
        with gzip.open(self.data_path + '.pickle.gz', 'rb') as f:
            data = pickle.load(f)
            self.keys = list(data.keys())
            for key in self.keys:
                ts = data[key]
                ts.index = pd.DatetimeIndex(ts.index)
                data[key] = ts.iloc[since:]
            self.store = data

    def test_compute_func_V(self, func, *args, **kargs):
        F = '1D'
        if 'F' in kargs:
            F = kargs['F']
            del kargs['F']
        else:
            print('No frenquency given, use F=', F)

        ts = self.store[self.keys[0]]  # V4K1H, V4K15M
        if self.name[0:2] == 'VL':
            ts = ts['volumes']

        if F in self.name:
            vols = ts.copy()
        else:
            vols = _resample_ts(ts, F, _merge_vols)

        res = pd.DataFrame([])
        key = 'test'
        func(key, vols, res, *args, **kargs)
        print("Test:", func.__name__)
        return res

    def test_compute_func_VL(self, func, *args, **kargs):
        F = '1D'
        if 'F' in kargs:
            F = kargs['F']
            del kargs['F']
        else:
            print('No frenquency given, use F=', F)

        ts = self.store[self.keys[0]]  # V4K1H, V4K15M
        if not isinstance(ts, pd.DataFrame):
            raise EnvironmentError(
                'This data source [%s] is not volumes+locations.' % self.name)

        if F in self.name:
            ts_v = ts['volumes'].copy()
            ts_l = ts['locations'].copy()
            ts_l[ts_l.isnull()] = '?'
        else:
            ts_v = _resample_ts(ts['volumes'], F, _merge_vols)
            ts_l = _resample_ts(ts['locations'], F, _merge_locations)

        res = pd.DataFrame([])
        key = 'test'
        func(key, ts_v, ts_l, res, *args, **kargs)
        print("Test:", func.__name__)
        return res

    def runVMPI(self, compute_func, F, n=None, parallel=False, chunksize=3, dump=False, **kwargs):
        import sys
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        nodes = comm.Get_size()
        rank = comm.Get_rank()
        if rank == 0:
            print("MPI mode:", nodes, "nodes.")
            sys.stdout.flush()

        if n is not None:
            n = min(len(self.keys), n)
            keys = self.keys[0:n]
        else:
            n = len(self.keys)
            keys = self.keys

        nodes = min(n, nodes)
        tasks = np.array_split(keys, nodes)

        dt = pd.DataFrame([], index=tasks[rank]) if rank < nodes else pd.DataFrame([])

        if rank < nodes:
            node_keys = tasks[rank]
            for key in node_keys:
                ts = self.store[key]  # V4K1H, V4K15M
                if self.name[0:2] == 'VL':
                    ts = ts['volumes']

                ts = _resample_ts(ts, F, _merge_vols)
                compute_func(key, ts, dt, **kwargs)

        print('Node', rank, 'is done.')
        sys.stdout.flush()
        all_dt = comm.gather(dt, root=0)
        if rank == 0:
            dt = pd.concat(all_dt)
            dt = dt.reindex(keys)
            dt = dt.reindex_axis(sorted(dt.columns), axis=1)
            if dump:
                dump_path = f'./result/{self.prefix}{self.name}_{compute_func.__name__}.csv.gz'
                print("Dump to", dump_path)
                dt.to_csv(dump_path, compression='gzip')
            return dt
        else:
            return pd.DataFrame([])

    def runV(self, compute_func, F, n=None, parallel=False, chunksize=3, dump=False, **kwargs):
        if n is not None:
            keys = self.keys[0:min(len(self.keys), n)]
        else:
            keys = self.keys

        if parallel:
            _func = partial(_parallel_compute_func_wrapper, compute_func=compute_func, mode='v')
            pool = Pool()
            tasks = []
            for key in keys:
                ts = self.store[key]  # V4K1H, V4K15M
                if self.name[0:2] == 'VL':
                    ts = ts['volumes']
                tasks.append((key, ts, kwargs))

            results = pool.imap_unordered(_func, tasks, chunksize=chunksize)

            pool.close()
            dt = pd.concat([r for r in self.tqdm(results, total=len(keys))])
            pool.join()
            del pool
        else:
            dt = pd.DataFrame([], index=keys)

            for key in self.tqdm(keys, desc='Running'):
                ts = self.store[key]  # V4K1H, V4K15M
                if self.name[0:2] == 'VL':
                    ts = ts['volumes']

                ts = _resample_ts(ts, F, _merge_vols)
                compute_func(key, ts, dt, **kwargs)

        dt = dt.reindex(keys)
        dt = dt.reindex_axis(sorted(dt.columns), axis=1)
        if dump:
            dump_path = f'./result/{self.prefix}{self.name}_{compute_func.__name__}.csv.gz'
            print("Dump to", dump_path)
            dt.to_csv(dump_path, compression='gzip')
        return dt

    def runVLMPI(self, compute_func, F, n=None, parallel=False, chunksize=3, dump=False, **kwargs):
        import sys
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        nodes = comm.Get_size()
        rank = comm.Get_rank()
        if rank == 0:
            print("MPI mode:", nodes, "nodes.")
            sys.stdout.flush()

        if n is not None:
            n = min(len(self.keys), n)
            keys = self.keys[0:n]
        else:
            n = len(self.keys)
            keys = self.keys

        nodes = min(n, nodes)
        tasks = np.array_split(keys, nodes)

        dt = pd.DataFrame([], index=tasks[rank]) if rank < nodes else pd.DataFrame([])

        if rank < nodes:
            node_keys = tasks[rank]
            for key in node_keys:
                ts = self.store[key]  # V4K1H, V4K15M
                if not isinstance(ts, pd.DataFrame):
                    raise EnvironmentError(
                        'This data source [%s] is not volumes+locations.' % self.name)

                if F in self.name:
                    ts_v = ts['volumes'].copy()
                    ts_l = ts['locations'].copy()
                    ts_l[ts_l.isnull()] = '?'
                else:
                    ts_v = _resample_ts(ts['volumes'], F, _merge_vols)
                    ts_l = _resample_ts(ts['locations'], F, _merge_locations)

                compute_func(key, ts_v, ts_l, dt, **kwargs)

        print('Node', rank, 'is done.')
        sys.stdout.flush()
        all_dt = comm.gather(dt, root=0)
        if rank == 0:
            dt = pd.concat(all_dt)
            dt = dt.reindex(keys)
            dt = dt.reindex_axis(sorted(dt.columns), axis=1)
            if dump:
                dump_path = f'./result/{self.prefix}{self.name}_{compute_func.__name__}.csv.gz'
                print("Dump to", dump_path)
                dt.to_csv(dump_path, compression='gzip')
            return dt
        else:
            return pd.DataFrame([])

    def runVL(self, compute_func, F, n=None, parallel=False, chunksize=3, dump=False, **kwargs):
        if n is not None:
            keys = self.keys[0:min(len(self.keys), n)]
        else:
            keys = self.keys

        if parallel:
            _func = partial(_parallel_compute_func_wrapper, compute_func=compute_func, mode='vl')
            pool = Pool()
            tasks = []
            for key in keys:
                ts = self.store[key]  # V4K1H, V4K15M
                if not isinstance(ts, pd.DataFrame):
                    raise EnvironmentError(
                        'This data source [%s] is not volumes+locations.' % self.name)

                if F in self.name:
                    ts_v = ts['volumes'].copy()
                    ts_l = ts['locations'].copy()
                    ts_l[ts_l.isnull()] = '?'
                else:
                    ts_v = _resample_ts(ts['volumes'], F, _merge_vols)
                    ts_l = _resample_ts(ts['locations'], F, _merge_locations)
                tasks.append((key, ts_v, ts_l, kwargs))

            results = pool.imap_unordered(_func, tasks, chunksize=chunksize)

            pool.close()
            dt = pd.concat([r for r in self.tqdm(results, total=len(keys))])
            pool.join()
            del pool
        else:
            dt = pd.DataFrame([], index=keys)

            for key in self.tqdm(keys, desc='Running'):
                ts = self.store[key]  # V4K1H, V4K15M
                if not isinstance(ts, pd.DataFrame):
                    raise EnvironmentError(
                        'This data source [%s] is not volumes+locations.' % self.name)

                if F in self.name:
                    ts_v = ts['volumes'].copy()
                    ts_l = ts['locations'].copy()
                    ts_l[ts_l.isnull()] = '?'
                else:
                    ts_v = _resample_ts(ts['volumes'], F, _merge_vols)
                    ts_l = _resample_ts(ts['locations'], F, _merge_locations)
                compute_func(key, ts_v, ts_l, dt, **kwargs)

        dt = dt.reindex(keys)
        dt = dt.reindex_axis(sorted(dt.columns), axis=1)
        if dump:
            dump_path = f'./result/{self.prefix}{self.name}_{compute_func.__name__}.csv.gz'
            print("Dump to", dump_path)
            dt.to_csv(dump_path, compression='gzip')
        return dt
