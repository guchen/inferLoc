cdef int _lambda(list time_sequence, int pos, int last=0):
    """the length of the shortest substring starting at position i which doesn't previously appear from position 1 to i-1.
    """
    cdef:
        int n = len(time_sequence), end
        str _SEP = '|*|', str_before = _SEP.join(time_sequence[0:pos]), str_after

    # optimized
    end = pos + 1 if last <= 2 else pos + last - 1
    while end < n + 1:
        str_after = _SEP.join(time_sequence[pos:end])
        if str_after not in str_before:
            return end - pos
        else:
            end += 1
    return 0

def lz(seq):
    # Estimated entropy rate using the Lempel-Ziv data compression
    # item in seq should be able to str() uniformly.
    # etp_rate = log(n) / (_sum * 1.0 / n)
    # to have a united logarithm, only return n, _sum
    cdef:
        int n = len(seq), last = 1
        list _seq = map(str, seq)
        float _sum = 1.0

    for i in range(1, n):
        last = _lambda(_seq, i, last)
        if last == 0:
            break
        else:
            _sum += last
    return n, _sum
