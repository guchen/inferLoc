from forecast.mlp import ts_l_to_mlp_XY, ts_v_to_mlp_XY, ts_vl_to_mlp_XY
from forecast.baseline import vols_to_mags
import numpy as np
from forecast.pytorch import train
from forecast.util import *
from sklearn.model_selection import ParameterGrid
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

torch.manual_seed(19881102)

def _mlp_XY_to_rnn_XY(X, Y, order):
    width = int((X.shape[1] + 1) / (order + 1))
    n_samples = X.shape[0]
    X = np.hstack((X, np.ones((X.shape[0], 1)) * (-1)))  # add a null row
    X = X.reshape(n_samples, order + 1, width)
    Y = Y.reshape(-1)
    return X, Y


def ts_v_to_rnn_XY(ts_v, order=2, locs=None, top_loc=15):
    X, Y = ts_v_to_mlp_XY(ts_v, order=order, locs=locs, top_loc=top_loc)
    return _mlp_XY_to_rnn_XY(X, Y, order)


class RNN(torch.nn.Module):
    def __init__(self, input_dim, output_dim, layout=(20, 10)):
        super(RNN, self).__init__()
        self.hidden_dim, self.num_layer = layout
        self.lstm = nn.RNN(input_dim, self.hidden_dim,
                           self.num_layer, batch_first=True, nonlinearity='relu', dropout=0.1)
        self.linear1 = nn.Linear(self.hidden_dim, output_dim, bias=True)
        self.softmax = nn.LogSoftmax(dim=0)

    def forward(self, x):
        batch_size = x.size()[0]
        fx, _ = self.lstm.forward(x)
        # fx shape is (n_sample, n_t, n_features) so fx[:,-1,:]
        y = self.linear1.forward(fx[:, -1, :])
        # y is 2d now
        y = self.softmax(y)
        return y


class RNNClassifier():
    def __init__(self, verbose=False, batch_size=24 * 5, layout=(20, 10), learning_rate=0.01, max_iter=400, alpha=1e-5):
        self.model = None
        self.layout = layout
        self.learning_rate = learning_rate
        self.max_iter = max_iter
        self.alpha = alpha
        self.batch_size = batch_size
        self.verbose = verbose

    def _build_model(self, n_features, n_classes):
        self.model = RNN(n_features, n_classes, self.layout)

    def fit(self, X_train, Y_train):
        if self.model is None:
            n_features, n_classes = X_train.shape[2], Y_train.shape[1]
            self._build_model(n_features, n_classes)

        n_examples = X_train.shape[0]
        X_train = torch.from_numpy(X_train).float()
        Y_train = torch.from_numpy(Y_train).float()

        batches = []
        batch_size = self.batch_size
        num_batches = n_examples // batch_size
        for k in range(num_batches):
            start, end = k * batch_size, (k + 1) * batch_size
            _X, _Y = X_train[start:end], Y_train[start:end]
            batches.append((_X, _Y))

        model = self.model
        loss = nn.MSELoss(size_average=True)
        optimizer = optim.Adam(
            model.parameters(), lr=self.learning_rate, weight_decay=self.alpha)

        cost1 = np.inf
        for i in range(self.max_iter):
            cost = train(model, loss, optimizer, batches)
            if self.verbose:
                print(("Epoch:", i, "Loss:", cost))
            if i > 40 and ((cost > cost1) or (cost1 - cost <= 1e-4)):
                break
            else:
                cost1 = cost

        return self

    def predict(self, X_test):
        X_test = torch.from_numpy(X_test).float()
        X_test = Variable(X_test, requires_grad=False)
        Y_predict = self.model.forward(X_test).data.cpu().numpy()
        return Y_predict


def _rnn_sequential_predictor(X, Y, n_predict, n_update, scale=False, verbose=False, **kwargs):
    if scale:
        shape = X.shape
        X = StandardScaler().fit_transform(
            X.reshape(shape[0], -1)).reshape(shape)

    Y_test = Y[-n_predict:]

    encoder = OneHotEncoder(sparse=False).fit(
        np.arange(0, Y.max() + 1).reshape(-1, 1))
    Y = encoder.transform(Y.reshape(-1, 1))
    if Y.shape[1] == 1:  # means only one state at all time
        # 100% prediction
        return Y_test, Y_test

    hidden_size  = max(5,int(2*(Y.shape[1]+X.shape[1])/3))
    model = RNNClassifier(layout=(hidden_size, 2), batch_size=10 * 24,
                          verbose=verbose, **kwargs).fit(X[0:-n_predict], Y[0:-n_predict])

    Y_predict = []
    n_total = X.shape[0]
    n_start = n_total - n_predict
    while n_start < n_total:
        n_end = min(n_total, n_start + n_update)
        _Y_predict = model.predict(X[n_start:n_end]).argmax(
            axis=1).astype(float).tolist()
        Y_predict += _Y_predict
        model = model.fit(X[0:n_end], Y[0:n_end])
        n_start += n_update
    return Y_predict, Y_test


def rnn_mags_sequential_predictor(mags_seq, n_predict, n_update, order, scale=True, verbose=False, **kwargs):
    X, Y = ts_v_to_rnn_XY(mags_seq, order=order)

    Y_predict, Y_test = _rnn_sequential_predictor(
        X, Y, n_predict, n_update, scale, verbose, **kwargs)
    return Y_predict


def compute_rnn_volume(phone, ts_v, results, n_predict, n_update, order=4, grid=False, verbose=False):
    mags = vols_to_mags(ts_v)
    vols_test = ts_v.values[-n_predict:]

    params = {
        'scale': [True],
        'learning_rate': [0.0001],
        'alpha': [1e-2,1e-3,1e-5] if grid else [1e-3]
    }

    paramgrid = list(ParameterGrid(params))
    n_jobs = 1

    i = 0
    tasks = []
    pool = Pool(n_jobs)
    for kwargs in paramgrid:
        results.loc[phone, 'params-%d' % i] = str(kwargs)

        kwargs = dict(n_predict=n_predict, n_update=n_update,
                      order=order, verbose=verbose, **kwargs)
        hook = pool.apply_async(
            rnn_mags_sequential_predictor, (mags,), kwargs)
        tasks.append(hook)
        i += 1
    pool.close()

    accu_max = -100.
    for i in range(len(tasks)):
        mags_predict = tasks[i].get()
        compute_rmse(phone, vols_test, results, mags_predict,
                     'mlp-mags-params-%d' % i, 'mags')
        accu_cur = results.loc[phone, 'accu-mlp-mags-params-%d' % i]
        if accu_cur > accu_max:
            accu_max = accu_cur
            results.loc[phone, 'best-param'] = int(i)
            compute_rmse(phone, vols_test, results, mags_predict,
                         'mlp-mags', 'mags')
    pool.join()
    del pool
