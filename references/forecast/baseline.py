# baseline
# predictability
from itertools import product

from forecast import entropy
from forecast.data import *
from forecast.predictor.base import dictkeymax
from forecast.predictor.markov import Seq2SeqMarkovPredictor, PPMPredictor
from forecast.predictor.textcompress import SPMPredictor, ALZPredictor
from forecast.util import compute_accu, Parallel, delayed, cpu_count, Pool


def ts_to_baseline_XY(array_1D, order, successive=True, sep=None):
    X = []
    Y = []

    N = len(array_1D)
    for i in range(order, N):
        if not successive and (i % sep) < order:
            continue
        x = array_1D[i - order:i]
        y = array_1D[i]
        X.append(x)
        Y.append(y)
    return X, Y


def _markov_sequential_predictor(seq, n_predict, order, method='markov'):
    _predictor = None
    if method == 'markov':
        _predictor = Seq2SeqMarkovPredictor(order=order)
    elif method == 'PPM':
        _predictor = PPMPredictor(order=order)

    Y_predict = _predictor.fit_predict(seq, n_predict)
    return Y_predict


def _text_sequential_predictor(mags_seq, n_predict, method='spm'):
    seq = mags_seq.tolist()
    if method == 'spm':
        Y_predict = SPMPredictor(0.5).fit_predict(seq, n_predict)
    elif method == 'alz':
        Y_predict = ALZPredictor().fit_predict(seq, n_predict)
    else:
        raise EnvironmentError('method is spm or alz.')

    return Y_predict


def _basic_sequential_predicts(phone, seq, results, n_predict):
    seq_test = seq[-n_predict:]
    # markov
    markov_orders = [1, 2, 3, 4, 5]
    markov_methods = ['PPM', 'markov']
    markov_settings = list(product(markov_orders, markov_methods))
    seq_predicts = Parallel(n_jobs=min(cpu_count(), len(markov_settings)))(delayed(_markov_sequential_predictor)(
        seq, n_predict, order, method=method) for order, method in markov_settings)

    for seq_predict, setting in zip(seq_predicts, markov_settings):
        order, method = setting
        compute_accu(phone, seq_test, results, seq_predict, f'{method}-{order}')

    # SPM
    alphas = [0.1, 0.25, 0.5, 0.75, 0.9]
    _seq = seq.tolist()
    for alpha in alphas:
        seq_predict = SPMPredictor(alpha).fit_predict(_seq, n_predict)
        compute_accu(phone, seq_test, results, seq_predict, f'SPM-{alpha}')

    # alz
    seq_predict = ALZPredictor().fit_predict(_seq, n_predict)
    compute_accu(phone, seq_test, results, seq_predict, f'ALZ')

    # last seen
    seq_predict = seq[(-n_predict - 1):-1]
    compute_accu(phone, seq_test, results, seq_predict, 'lastseen')

    # onlyzero
    seq_predict = np.zeros(n_predict)
    compute_accu(phone, seq_test, results, seq_predict, 'onlyzero')


def ts_l_to_locs(ts_l, top_loc=15):
    locs = ts_l.copy()
    keys = locs.value_counts().keys()
    locs = locs.apply(keys.get_loc)
    if top_loc is not None:
        locs[locs >= top_loc] = top_loc
    return locs


def ts_v_to_mags(ts_v):
    return vols_to_mags(ts_v)


def _compute_baseline_single_behavior(phone, states, results, n_predict):
    # predictability
    etp = entropy.entropy_rate(states)
    n_classes = len(np.unique(states))
    p_max = entropy.predictability_max(etp, n_classes)
    p_min = entropy.predictability_min_erate(etp, n_classes)
    results.loc[phone, 'etp'] = etp
    results.loc[phone, 'p-max'] = p_max
    results.loc[phone, 'p-min'] = p_min
    results.loc[phone, 'n-locs'] = n_classes

    # predicts
    _basic_sequential_predicts(phone, states, results, n_predict)


def compute_baseline_location(phone, ts_v, ts_l, results, n_predict):
    locs = ts_l_to_locs(ts_l, top_loc=15)
    _compute_baseline_single_behavior(phone, locs, results, n_predict)


def compute_baseline_volume(phone, ts_v, results, n_predict):
    mags = ts_v_to_mags(ts_v)
    _compute_baseline_single_behavior(phone, mags, results, n_predict)


def _joint_sequential_predict(model, mags, locs, n_predict):
    states = mags.astype(int).astype(str) + '|' + locs.astype(int).astype(str)

    # joint predict
    def separate(state):
        _arr = state.split('|')
        return int(_arr[0]), int(_arr[1])

    Y_proba = model.fit_predict_proba(states, n_predict)

    Y_predict_joint = [dictkeymax(proba) for proba in Y_proba]
    mags_predict = []
    locs_predict = []
    for state in Y_predict_joint:
        mag, loc = separate(state)
        mags_predict.append(mag)
        locs_predict.append(loc)

    proba_table = []
    for i in range(len(Y_proba)):
        proba = Y_proba[i]
        for symbol in proba:
            proba_table.append(separate(symbol) + (i, proba[symbol]))
    proba_table = pd.DataFrame(proba_table, columns=['mags', 'locs', 'id', 'probs'])
    mags_predict_cum = proba_table.groupby(['id', 'mags']).sum().reset_index('id').groupby('id').apply(
        lambda _dt: _dt['probs'].idxmax()).sort_index().values
    locs_predict_cum = proba_table.groupby(['id', 'locs']).sum().reset_index('id').groupby('id').apply(
        lambda _dt: _dt['probs'].idxmax()).sort_index().values

    return mags_predict, locs_predict, mags_predict_cum, locs_predict_cum


def compute_baseline_volume_location(phone, ts_v, ts_l, results, n_predict):
    locs = ts_l_to_locs(ts_l, top_loc=15)
    mags = vols_to_mags(ts_v)
    states = mags.astype(int).astype(str) + '|' + locs.astype(int).astype(str)
    # predictability
    etp = entropy.entropy_rate(states)
    n_classes = len(np.unique(states))
    p_max = entropy.predictability_max(etp, n_classes)
    p_min = entropy.predictability_min_erate(etp, n_classes)
    results.loc[phone, 'etp'] = etp
    results.loc[phone, 'p-max'] = p_max
    results.loc[phone, 'p-min'] = p_min
    results.loc[phone, 'n-states'] = n_classes

    seq_test = states[-n_predict:]
    ## sep
    # markov
    mags_predict = _markov_sequential_predictor(mags,n_predict,order=2,method='markov')
    locs_predict = _markov_sequential_predictor(locs,n_predict,order=2,method='markov')
    Y_predict_joint = pd.Series(mags_predict).astype(int).astype(str) + '|' + pd.Series(locs_predict).astype(int).astype(str)
    compute_accu(phone, seq_test, results, Y_predict_joint, 'markov-2-sep')
    # ppm
    mags_predict = _markov_sequential_predictor(mags,n_predict,order=2,method='PPM')
    locs_predict = _markov_sequential_predictor(locs,n_predict,order=2,method='PPM')
    Y_predict_joint = pd.Series(mags_predict).astype(int).astype(str) + '|' + pd.Series(locs_predict).astype(int).astype(str)
    compute_accu(phone, seq_test, results, Y_predict_joint, 'PPM-2-sep')
    # alz
    mags_predict = _text_sequential_predictor(mags,n_predict,method='alz')
    locs_predict = _text_sequential_predictor(locs,n_predict,method='alz')
    Y_predict_joint = pd.Series(mags_predict).astype(int).astype(str) + '|' + pd.Series(locs_predict).astype(int).astype(str)
    compute_accu(phone, seq_test, results, Y_predict_joint, 'ALZ-sep')
    ## sep end

    # last seen
    seq_predict = states[(-n_predict - 1):-1]
    compute_accu(phone, seq_test, results, seq_predict, 'lastseen')

    # onlyzero
    seq_predict = ['0|0'] * n_predict
    compute_accu(phone, seq_test, results, seq_predict, 'onlyzero')

    mags_test = mags[-n_predict:]
    locs_test = locs[-n_predict:]

    models = {
        'alz': ALZPredictor(),
        # 'spm': SPMPredictor(0.5), #TODO: fix the bug,  spm only accepts string stream
        'PPM-1': PPMPredictor(order=1),
        'PPM-2': PPMPredictor(order=2),
        # 'PPM-3': PPMPredictor(order=3),
        # 'PPM-4': PPMPredictor(order=4),
        # 'PPM-5': PPMPredictor(order=5),
        'markov-1': Seq2SeqMarkovPredictor(order=1),
        'markov-2': Seq2SeqMarkovPredictor(order=2),
        # 'markov-3': Seq2SeqMarkovPredictor(order=3), # too slow
        # 'markov-4': Seq2SeqMarkovPredictor(order=4),
    }

    tasks = []
    pool = Pool()

    for method in models:
        model = models[method]
        hook = pool.apply_async(_joint_sequential_predict, (model, mags, locs, n_predict))
        tasks.append((method, hook))

    pool.close()

    for method, hook in tasks:
        mags_predict, locs_predict, mags_predict_cum, locs_predict_cum = hook.get()
        Y_predict_joint = pd.Series(mags_predict).astype(int).astype(str) + '|' + pd.Series(locs_predict).astype(
            int).astype(str)
        compute_accu(phone, seq_test, results, Y_predict_joint, method + '-joint')
        compute_accu(phone, mags_test, results, mags_predict, method + '-mags')
        compute_accu(phone, locs_test, results, locs_predict, method + '-locs')

        Y_predict_joint_cum = pd.Series(mags_predict_cum).astype(int).astype(str) + '|' + pd.Series(
            locs_predict_cum).astype(
            int).astype(str)
        compute_accu(phone, seq_test, results, Y_predict_joint_cum, method + '-joint-cum')
        compute_accu(phone, mags_test, results, mags_predict_cum, method + '-mags-cum')
        compute_accu(phone, locs_test, results, locs_predict_cum, method + '-locs-cum')

    pool.join()
    del pool


def _vols2labels(vols, mode=2):
    orders = np.ceil(np.log10(vols + 1.))
    if mode == 1:
        return np.ceil(orders / 3).astype('uint8', copy=False)
    elif mode == 2:
        return orders.astype('uint8', copy=False)
    elif mode == 3:
        # numpy.where(c,xv,yv)
        # [xv if c else yv for (c, xv, yv) in zip(condition, x, y)]
        orders_q3 = orders * 10 + np.floor((vols - 10 ** (orders - 1)) / (9 * 10 ** (orders - 1) / 2.0))
        return np.where(vols > 1000, orders_q3, orders).astype('uint8', copy=False)
    elif mode == 4:
        orders_q4 = orders * 10 + np.floor((vols - 10 ** (orders - 1)) / (9 * 10 ** (orders - 1) / 3.0))
        return np.where(vols > 1000, orders_q4, orders).astype('uint8', copy=False)
    elif mode == 5:
        orders_q5 = orders * 10 + np.floor((vols - 10 ** (orders - 1)) / (9 * 10 ** (orders - 1) / 9.0))
        return np.where(vols > 1000, orders_q5, orders).astype('uint8', copy=False)
    else:
        raise Exception('Mode is wrong.')


def _reasonable_n(ts_symbol):
    ts_symbol = ts_symbol.tolist()
    transition = pd.DataFrame([ts_symbol[0:-1], ts_symbol[1:]], index=['from', 'to']).T
    n_r = transition.groupby('from').apply(lambda _dt: len(_dt['to'].unique())).max()
    return max(2, n_r)


def compute_predictability_joint(phone, ts_v, ts_l, results):
    locs = ts_l_to_locs(ts_l, top_loc=1e5)
    mags = vols_to_mags(ts_v)
    states = mags.astype(int).astype(str) + '|' + locs.astype(int).astype(str)

    n_classes = len(pd.unique(states))
    n_r = _reasonable_n(states)
    etp = entropy.entropy_rate(states)
    etp_unc = entropy.entropy_tunc(states)
    n_v = len(pd.unique(mags))
    etp_l = entropy.entropy_rate(locs)
    etp_v = entropy.entropy_rate(mags)

    results.loc[phone, 'etp'] = etp
    results.loc[phone, 'etp-unc'] = etp_unc
    results.loc[phone, 'p-max'] = entropy.predictability_max(etp, n_classes)
    results.loc[phone, 'p-min'] = entropy.predictability_min_erate(etp, n_classes)
    results.loc[phone, 'p-unc'] = entropy.predictability_max(etp_unc, n_classes)
    results.loc[phone, 'p-eunc'] = entropy.predictability_max(etp_unc, n_r)
    results.loc[phone, 'n-locs'] = n_classes
    results.loc[phone, 'p-emax'] = entropy.predictability_max(etp, n_r)
    results.loc[phone, 'p-emin'] = entropy.predictability_min_erate(etp, n_r)
    results.loc[phone, 'n-r'] = n_r
    results.loc[phone, 'etp-l'] = etp_l
    results.loc[phone, 'etp-v'] = etp_v
    results.loc[phone, 'p-max-v-cond'] = entropy.predictability_max((etp-etp_l),n_v)


def compute_predictability_location(phone, ts_v, ts_l, results):
    # predictability
    unique_locs = ts_l.unique()
    if '?' in unique_locs:
        # have missing locations -> use old mode
        n_classes = len(unique_locs) - 1
        locs = ts_l
        etp = entropy.entropy_rate_location(locs, unknown_mask='?', return_cov=False)
        _locs = locs[locs != '?'].tolist()
        etp_unc = entropy.entropy_tunc(_locs)
        etp_rand = entropy.entropy_rand(_locs)
        n_r = None
    else:
        # complete trajectory -> use new mode
        n_classes = len(unique_locs)
        locs = ts_l_to_locs(ts_l, top_loc=1e5)
        etp = entropy.entropy_rate(locs) + 0.4
        etp_unc = entropy.entropy_tunc(locs)
        etp_rand = entropy.entropy_rand(locs)
        n_r = _reasonable_n(locs)

    results.loc[phone, 'etp'] = etp
    results.loc[phone, 'etp-unc'] = etp_unc
    results.loc[phone, 'etp-rand'] = etp_rand
    results.loc[phone, 'p-max'] = entropy.predictability_max(etp, n_classes)
    results.loc[phone, 'p-min'] = entropy.predictability_min_erate(etp, n_classes)
    results.loc[phone, 'p-unc'] = entropy.predictability_max(etp_unc, n_classes)
    results.loc[phone, 'p-rand'] = entropy.predictability_max(etp_rand, n_classes)
    results.loc[phone, 'n-locs'] = n_classes
    if n_r is not None:
        results.loc[phone, 'p-emax'] = entropy.predictability_max(etp, n_r)
        results.loc[phone, 'n-r'] = n_r


def compute_predictability_volume(phone, ts_v, results):
    for min in [15, 30, 45, 60]:
        _ts_v = ts_v.resample(f'{min}T').sum().dropna()
        for q in range(1, 6):
            param = f'{min}M-Q{q}'
            _ts_v_q = _vols2labels(_ts_v, q)
            _n = len(np.unique(_ts_v_q))
            _etp = entropy.entropy_rate(_ts_v_q)
            results.loc[phone, f'etp-{param}'] = _etp
            results.loc[phone, f'p-max-{param}'] = entropy.predictability_max(_etp, _n)
            results.loc[phone, f'p-min-{param}'] = entropy.predictability_min_erate(_etp, _n)
            _etp_unc = entropy.entropy_tunc(_ts_v_q)
            results.loc[phone, f'etp-unc-{param}'] = _etp_unc
            results.loc[phone, f'p-unc-max-{param}'] = entropy.predictability_max(_etp_unc, _n)
            _etp_rand = entropy.entropy_rand(_ts_v_q)
            results.loc[phone, f'etp-rand-{param}'] = _etp_rand
            results.loc[phone, f'p-rand-max-{param}'] = entropy.predictability_max(_etp_rand, _n)
            results.loc[phone, f'n-{param}'] = _n
            _n_r = _reasonable_n(_ts_v_q)
            results.loc[phone, f'n_r-{param}'] = _n_r
            results.loc[phone, f'p-emax-{param}'] = entropy.predictability_max(_etp, _n_r)
