import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

# cuda = torch.cuda.is_available()
cuda = False
# cuda = False
# print(("CUDA:", cuda))


def train(model, criterion, optimizer, batches):
    # switch to train mode
    model.train()

    err = 0.
    for X, Y in batches:
        if cuda:
            Y = Y.cuda(async=True)
        input_var = torch.autograd.Variable(X)
        target_var = torch.autograd.Variable(Y)

        # compute output
        output = model(input_var)
        loss = criterion(output, target_var)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        err += loss.data[0]
    return err


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = 0.01 * (0.1 ** (epoch // 50))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def torchRegressor(X_train, Y_train, X_test, batch_size=1, verbose=False):
    global cuda
    n_examples, n_features = X_train.shape
    model = nn.Sequential(
        nn.Linear(n_features, 5, bias=True),
        nn.Linear(5, 5),
        nn.ReLU(),
        nn.Linear(5, 5),
        nn.Linear(5, 1, bias=True)
    )
    if cuda:
        model = torch.nn.DataParallel(model).cuda()

    X_train = torch.from_numpy(X_train).float()
    Y_train = torch.from_numpy(Y_train.reshape(-1, 1)).float()
    X_test = torch.from_numpy(X_test).float()

    batches = []
    num_batches = n_examples // batch_size
    for k in range(num_batches):
        start, end = k * batch_size, (k + 1) * batch_size
        _X, _Y = X_train[start:end], Y_train[start:end]
        if cuda:
            _X, _Y = _X.cuda(async=True), _Y.cuda(async=True)
        batches.append((_X, _Y))

    loss = nn.MSELoss(size_average=True)
    if loss:
        loss = loss.cuda()

#     optimizer = optim.ASGD(model.parameters())
    optimizer = optim.SGD(model.parameters(), lr=0.001,
                          momentum=0.9, weight_decay=1e-3)

    cost1 = np.inf
    for i in range(201):
        #         adjust_learning_rate(optimizer,i)
        cost = train(model, loss, optimizer, batches)
        if verbose and i % 20 == 0:
            print(("Epoch:", i, "Loss:", cost))
        if np.abs(cost - cost1) <= 1e-3:
            break
        else:
            cost1 = cost

    X_test = Variable(X_test, requires_grad=False)
    if cuda:
        X_test = X_test.cuda(async=True)
    Y_predict = model.forward(X_test).data.cpu().numpy().reshape(-1)
    return Y_predict

from sklearn.preprocessing import LabelEncoder


def torchClassifier(X_train, Y_train, X_test, batch_size=1, verbose=False):
    global cuda
    encoder = LabelEncoder().fit(Y_train)
    Y_train = encoder.transform(Y_train)

    n_examples, n_features = X_train.shape
    n_classes = np.unique(Y_train).size
    model = nn.Sequential(
        nn.Linear(n_features, 50, bias=True),
        nn.ReLU(),
        nn.Linear(50,n_classes,bias=True),
        nn.Softmax()
    )

    if cuda:
        model = torch.nn.DataParallel(model).cuda()

    X_train = torch.from_numpy(X_train).float()
    Y_train = torch.from_numpy(Y_train.reshape(-1)).long()
    X_test = torch.from_numpy(X_test).float()

    batches = []
    num_batches = n_examples // batch_size
    for k in range(num_batches):
        start, end = k * batch_size, (k + 1) * batch_size
        _X, _Y = X_train[start:end], Y_train[start:end]
        if cuda:
            _X, _Y = _X.cuda(async=True), _Y.cuda(async=True)
        batches.append((_X, _Y))

    loss = nn.CrossEntropyLoss(size_average=False)
#     optimizer = optim.ASGD(model.parameters(),lr=0.1)
    # optimizer = optim.SGD(model.parameters(), lr=0.05,
                        #   momentum=0.9, weight_decay=1e-3)
    optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=0.0001)

    cost1 = np.inf
    for i in range(200):
        #         adjust_learning_rate(optimizer,i)
        cost = train(model, loss, optimizer, batches)
        if verbose and i % 20 == 0:
            print(("Epoch:", i, "Loss:", cost))
        if (cost > cost1) or (cost1 - cost <= 1e-4):
            break
        else:
            cost1 = cost

    X_test = Variable(X_test, requires_grad=False)
    if cuda:
        X_test = X_test.cuda(async=True)
    Y_predict = model.forward(X_test).data.cpu().numpy().argmax(axis=1).reshape(-1)
    return encoder.inverse_transform(Y_predict)
