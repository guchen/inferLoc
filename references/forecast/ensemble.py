from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import ParameterGrid
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
import numpy as np

from forecast.mlp import ts_l_to_mlp_XY, ts_v_to_mlp_XY, ts_vl_to_mlp_XY, _TSD_to_XY_align_time, \
    _ts_index_to_time_features
from forecast.baseline import ts_to_baseline_XY, ts_l_to_locs, ts_v_to_mags
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from forecast.data import base_dir
from forecast.util import *

# POI
import pickle
import gzip

with gzip.open(base_dir + '/../data/VL7k150d/POIs.pickle.gz', 'r') as o:
    POIs = pickle.load(o)
    cats = []
    for loc in POIs:
        for _cats in POIs[loc]:
            cats += _cats
    cats = pd.unique(cats)


def location_vec(location_str, debug=False):
    # fix
    location_str = location_str.replace('|', ',')
    global POIs, cats
    pois = POIs[location_str]
    dt = pd.DataFrame([{cat: 1 for cat in poi}
                       for poi in pois], columns=cats).fillna(0.)
    if debug:
        return dt
    if len(dt.index) > 0:
        return dt.mean().values.reshape(-1)
    else:
        return np.zeros(len(cats)).reshape(-1)


# POI End

def ts_v_to_ensemble_XY1(ts_v, order=2, locs=None, top_loc=15):
    sep = np.unique(np.floor(ts_v.values)).mean() + 0.9
    diffs = list(map(lambda v: 1 if v > sep else -1, ts_v.values.tolist()))
    diffs = np.array(diffs).reshape(-1, 1)

    ts_v.index = pd.DatetimeIndex(ts_v.index)
    time_features = _ts_index_to_time_features(ts_v.index)
    vols = ts_v.values.reshape(-1, 1)
    TSD = np.hstack((diffs,) + time_features + (vols,))
    # TSD = np.hstack(time_features + (vols,))
    if locs is not None:
        keys = locs.value_counts().keys()
        locs = locs.apply(keys.get_loc)
        if top_loc is not None:
            locs[locs >= top_loc] = top_loc
        locs = locs.values.reshape(-1, 1)
        TSD = np.hstack((locs, diffs) + time_features + (vols,))

    return _TSD_to_XY_align_time(TSD, order)


def ts_v_to_ensemble_XY(ts_v, order=2, locs=None):
    # sep = np.unique(np.floor(ts_v.values)).mean() + 0.7
    # diffs = list(map(lambda v: 1 if v > sep else -1, ts_v.values.tolist()))
    # diffs = np.array(diffs).reshape(-1, 1)

    ts_v.index = pd.DatetimeIndex(ts_v.index)
    # time_features = _ts_index_to_time_features(ts_v.index)
    vols = ts_v.values.reshape(-1, 1)
    TSD = np.hstack((vols,))
    # TSD = np.hstack((diffs,) + time_features + (vols,))
    if locs is not None:
        locs_vec = []
        for _, loc_str in locs.iteritems():
            locs_vec.append(location_vec(loc_str).tolist())
        locs_vec = np.array(locs_vec)
        locs_vec = locs_vec[:, ~np.all(locs_vec == 0, axis=0)]

        TSD = np.hstack((locs_vec, vols))

    X = []
    n_samples, width = TSD.shape
    for i in range(order, n_samples):
        X.append(TSD[i - order:i + 1])
    X = np.array(X).reshape(len(X), -1)
    Y = X[:, -1:]
    X = X[:, 0:-1]
    return X, Y
    # return _TSD_to_XY_align_time(TSD, order)


from sklearn.multioutput import MultiOutputClassifier
from sklearn.base import clone


def ensemble_mags_VL_sequential_predictor(mags_seq, locs_seq, n_predict, n_update, order, scale=True, simple=False,
                                          verbose=False, method='sgd', **kwargs):
    if simple:
        Xv, Yv = ts_to_baseline_XY(mags_seq, order=order)
        Xl, Yl = ts_to_baseline_XY(locs_seq, order=order)
        X = np.hstack((Xv, Xl))
        Yv = np.array(Yv)
        Yl = np.array(Yl)
        Y = np.vstack((Yv, Yl)).T
    else:
        X, Yv, Yl = ts_vl_to_mlp_XY(mags_seq, locs_seq, order=order)
        Y = np.hstack((Yv, Yl))

    Yv_test, Yl_test = Yv[-n_predict:], Yl[-n_predict:]

    if scale:
        scaler = StandardScaler().fit(X)
        X = scaler.transform(X)

    if method == 'sgd':
        model = SGDClassifier(random_state=19881102, verbose=verbose, warm_start=True,
                              max_iter=2000, tol=1e-3,
                              **kwargs)
    elif method == 'gbc':
        model = GradientBoostingClassifier(random_state=19881102, n_estimators=200, verbose=verbose, warm_start=True,
                                           **kwargs)
    elif method == 'rfc':
        n_estimators = 200
        model = RandomForestClassifier(random_state=19881102, n_estimators=n_estimators, verbose=verbose,
                                       max_features=None,
                                       warm_start=True,
                                       **kwargs)
    elif method == 'dtc':
        model = DecisionTreeClassifier(random_state=19881102, max_features=None,
                                       **kwargs)
    else:
        raise EnvironmentError('error')

    Yv_predict, Yl_predict = [], []
    n_total = X.shape[0]
    n_start = n_total - n_predict

    models = [clone(model) for _ in range(Y.shape[1])]

    _n_unique_before = [-1 for _ in range(Y.shape[1])]

    while n_start < n_total:
        _Y = []
        n_end = min(n_total, n_start + n_update)
        for i in range(Y.shape[1]):
            n_unique_before_i = len(np.unique(Y[0:n_start, i]))
            _n_unique_before_i = _n_unique_before[i]
            if n_unique_before_i == 1:
                _Y.append([Y[0, i] for _ in range(n_start, n_end)])
            else:
                if method in ['gbc', 'sgd', 'rfc']:
                    models[i].set_params(warm_start=(n_unique_before_i == _n_unique_before_i))
                if method == 'rfc':
                    n_estimators += 10
                    models[i].set_params(n_estimators=n_estimators)

                _n_unique_before[i] = n_unique_before_i
                models[i].fit(X[0:n_start], Y[0:n_start, i])
                _Y.append(models[i].predict(X[n_start:n_end]))

        Yv_predict.append(_Y[0])
        Yl_predict.append(_Y[1])
        n_start += n_update

    Yv_predict = np.concatenate(Yv_predict)
    Yl_predict = np.concatenate(Yl_predict)

    return Yv_predict, Yl_predict, Yv_test, Yl_test


def _ensemble_sequential_predictor(X, Y, n_predict, n_update, scale, verbose=False, method='sgd', **kwargs):
    if scale:
        X = StandardScaler().fit_transform(X)

    Y = Y.reshape(-1)
    Y_test = Y[-n_predict:]

    if method == 'sgd':
        model = SGDClassifier(random_state=19881102, verbose=verbose, warm_start=True,
                              max_iter=1000, tol=1e-3,
                              **kwargs)
    elif method == 'gbc':
        model = GradientBoostingClassifier(random_state=19881102, verbose=verbose, warm_start=True, **kwargs)
    elif method == 'rfc':
        n_estimators = 100
        model = RandomForestClassifier(random_state=19881102, n_estimators=n_estimators, verbose=verbose,
                                       max_features=None,
                                       warm_start=True,
                                       **kwargs)
    elif method == 'dtc':
        model = DecisionTreeClassifier(random_state=19881102, max_features=None,
                                       **kwargs)
    else:
        raise EnvironmentError('error')

    Y_predict = []
    n_total = X.shape[0]
    n_start = n_total - n_predict

    _n_unique_before = -1

    while n_start < n_total:
        n_unique_before = len(np.unique(Y[0:n_start]))
        n_end = min(n_total, n_start + n_update)
        if n_unique_before == 1:
            _Y_predict = [Y[0] for i in range(n_start, n_end)]
        else:
            if method in ['gbc', 'sgd', 'rfc']:
                model.set_params(warm_start=(n_unique_before == _n_unique_before))
            if method == 'rfc':
                n_estimators += 10
                model.set_params(n_estimators=n_estimators)
            _n_unique_before = n_unique_before
            model.fit(X[0:n_start], Y[0:n_start])
            _Y_predict = model.predict(X[n_start:n_end]).tolist()

        Y_predict += _Y_predict
        n_start += n_update

    return Y_predict, Y_test


def ensemble_mags_sequential_predictor(mags_seq, n_predict, n_update, order, locs=None, scale=True, simple=False,
                                       verbose=False, **kwargs):
    if simple and locs is None:
        X, Y = ts_to_baseline_XY(mags_seq, order=order)
        X, Y = np.array(X), np.array(Y)
    else:
        if locs is None:
            # X, Y = ts_v_to_mlp_XY(mags_seq, order=order)
            X, Y = ts_v_to_ensemble_XY1(mags_seq, order=order)
        else:
            X, Y = ts_v_to_ensemble_XY(mags_seq, order=order, locs=locs)

    Y_predict, Y_test = _ensemble_sequential_predictor(
        X, Y, n_predict=n_predict, n_update=n_update, scale=scale, verbose=verbose, **kwargs)
    return Y_predict


def ensemble_locs_sequential_predictor(locs_seq, n_predict, n_update, order, scale=True, simple=False, verbose=False,
                                       **kwargs):
    if simple:
        X, Y = ts_to_baseline_XY(locs_seq, order=order)
        X, Y = np.array(X).astype('float'), np.array(Y).astype('float')
    else:
        X, Y = ts_l_to_mlp_XY(locs_seq, order=order)

    Y_predict, Y_test = _ensemble_sequential_predictor(
        X, Y, n_predict=n_predict, n_update=n_update, scale=scale, verbose=verbose, **kwargs)
    return Y_predict, Y_test


def compute_ensemble_volume(phone, ts_v, results, n_predict, n_update, grid=False, verbose=False):
    mags = vols_to_mags(ts_v)
    vols_test = ts_v.values[-n_predict:]

    for method in ['sgd', 'gbc']:  # ['sgd', 'gbc', 'rfc', 'dtc']:
        for simple in [True, False]:
            params = {
                'simple': [simple],
                'scale': [False, True],
            }

            if grid:
                if method == 'sgd':
                    params['alpha'] = [0.0001, 0.001, 0.01, 0.1]
                    params['l1_ratio'] = [.15, .25, .5, .75, .85]
                elif method == 'gbc':
                    params['learning_rate'] = [1e-3, 1e-2, 1e-1, 1.]
                    params['max_depth'] = [3, 8, 12]
                elif method == 'rfc':
                    params['criterion'] = ['gini', 'entropy']
                elif method == 'dtc':
                    params['criterion'] = ['gini', 'entropy']

            for order in [1, 2, 3, 4, 5, 6, 7, 8]:
                prefix = f'{method}-mags-{order}'
                if simple:
                    prefix = 'simple-' + prefix
                # print(prefix)
                i = 0
                tasks = []
                for kwargs in ParameterGrid(params):
                    results.loc[phone, f'{prefix}-params-{i}'] = str(kwargs)

                    kwargs = dict(n_predict=n_predict, n_update=n_update,
                                  order=order, verbose=verbose, method=method, **kwargs)
                    args = (mags,)
                    tasks.append(
                        ensemble_mags_sequential_predictor(*args, **kwargs))
                    i += 1

                accu_max = -100.
                for i in range(len(tasks)):
                    mags_predict = tasks[i]
                    compute_rmse(phone, vols_test, results, mags_predict,
                                 f'{prefix}-params-{i}', 'mags')
                    accu_cur = results.loc[phone, f'accu-{prefix}-params-{i}']
                    if accu_cur > accu_max:
                        accu_max = accu_cur
                        results.loc[phone, f'best-param-{order}'] = int(i)
                        compute_rmse(phone, vols_test, results, mags_predict,
                                     f'{prefix}', 'mags')


def compute_ensemble_volume_location(phone, ts_v, ts_l, results, n_predict, n_update, grid=False, verbose=False):
    mags = vols_to_mags(ts_v)
    locs = ts_l_to_locs(ts_l, top_loc=15)

    for method in ['sgd', 'gbc']:  # ['sgd', 'gbc', 'rfc', 'dtc']:
        for simple in [True, False]:
            params = {
                'simple': [simple],
                'scale': [False, True],
            }

            if grid:
                if method == 'sgd':
                    params['alpha'] = [0.0001, 0.001, 0.01, 0.1]
                    params['l1_ratio'] = [.15, .25, .5, .75, .85]
                elif method == 'gbc':
                    params['learning_rate'] = [1e-3, 1e-2, 1e-1, 1.]
                    params['max_depth'] = [3, 8, 12]
                elif method == 'rfc':
                    params['criterion'] = ['gini', 'entropy']
                elif method == 'dtc':
                    params['criterion'] = ['gini', 'entropy']

            for order in [1, 2, 3, 4]:
                prefix = f'{method}'
                if simple:
                    prefix = 'simple-' + prefix

                ## sep
                if order == 4:
                    mags_predict = ensemble_mags_sequential_predictor(mags, n_predict=n_predict, n_update=n_update,
                                                                      simple=simple,
                                                                      scale=True, order=order, verbose=verbose)
                    locs_predict, locs_test = ensemble_locs_sequential_predictor(locs, n_predict=n_predict,
                                                                                 n_update=n_update,
                                                                                 simple=simple,
                                                                                 scale=True, order=order,
                                                                                 verbose=verbose)
                    mags_test = mags[-n_predict:].tolist()
                    accu = 0.
                    total = 0.
                    for j in range(len(mags_predict)):
                        total += 1
                        test_mag = (mags_predict[j] == mags_test[j])
                        test_loc = (locs_predict[j] == locs_test[j])
                        if test_mag and test_loc:
                            accu += 1
                    results.loc[phone, f'accu-{prefix}-sep'] = accu / total
                ## sep end

                i = 0
                tasks = []

                for kwargs in ParameterGrid(params):
                    results.loc[phone, f'{prefix}-params-{i}'] = str(kwargs)

                    kwargs = dict(n_predict=n_predict, n_update=n_update,
                                  order=order, verbose=verbose, method=method, **kwargs)

                    args = (mags, locs)
                    tasks.append(ensemble_mags_VL_sequential_predictor(*args, **kwargs))
                    i += 1

                accu_max = -100.
                for i in range(len(tasks)):
                    Yv_predict, Yl_predict, Yv_test, Yl_test = tasks[i]

                    total = accu_v = accu_l = accu_both = 0
                    for j in range(len(Yv_predict)):
                        total += 1
                        if Yv_predict[j] == Yv_test[j]:
                            accu_v += 1
                        if Yl_predict[j] == Yl_test[j]:
                            accu_l += 1
                        if (Yv_predict[j] == Yv_test[j]) and (Yl_predict[j] == Yl_test[j]):
                            accu_both += 1
                    accu_v /= total
                    accu_l /= total
                    accu_both /= total

                    results.loc[phone, f'accu-{prefix}-mags-{order}-params-{i}'] = accu_v
                    results.loc[phone, f'accu-{prefix}-locs-{order}-params-{i}'] = accu_l

                    accu_cur = accu_v + accu_l
                    if accu_cur > accu_max:
                        accu_max = accu_cur
                        results.loc[phone, f'{prefix}-{order}-best-param'] = int(i)
                        results.loc[phone, f'accu-{prefix}-mags-{order}'] = accu_v
                        results.loc[phone, f'accu-{prefix}-locs-{order}'] = accu_l
                        results.loc[phone, f'accu-{prefix}-both-{order}'] = accu_both


def compute_ensemble_volume_with_location(phone, ts_v, ts_l, results, n_predict, n_update, grid=False, verbose=False):
    mags = vols_to_mags(ts_v)
    vols_test = ts_v.values[-n_predict:]

    for method in ['sgd', 'gbc']:  # ['sgd', 'gbc', 'rfc', 'dtc']:
        params = {
            'scale': [False, True],
        }

        if grid:
            if method == 'sgd':
                params['alpha'] = [0.0001, 0.001, 0.01, 0.1]
                params['l1_ratio'] = [.15, .25, .5, .75, .85]
            elif method == 'gbc':
                params['learning_rate'] = [1e-3, 1e-2, 1e-1, 1.]
                params['max_depth'] = [3, 8, 12]
            elif method == 'rfc':
                params['criterion'] = ['gini', 'entropy']
            elif method == 'dtc':
                params['criterion'] = ['gini', 'entropy']

        for order in [1, 2, 3, 4, 5, 6, 7, 8]:
            prefix = f'{method}withL-mags-{order}'
            # print(prefix)

            i = 0
            tasks = []
            for kwargs in ParameterGrid(params):
                results.loc[phone, f'{prefix}-params-{i}'] = str(kwargs)

                kwargs = dict(n_predict=n_predict, n_update=n_update, locs=ts_l,
                              order=order, verbose=verbose, method=method, **kwargs)
                args = (mags,)
                tasks.append(
                    ensemble_mags_sequential_predictor(*args, **kwargs))
                i += 1

            accu_max = -100.
            for i in range(len(tasks)):
                mags_predict = tasks[i]
                compute_rmse(phone, vols_test, results, mags_predict,
                             f'{prefix}-params-{i}', 'mags')
                accu_cur = results.loc[phone, f'accu-{prefix}-params-{i}']
                if accu_cur > accu_max:
                    accu_max = accu_cur
                    results.loc[phone, f'best-param-{order}'] = int(i)
                    compute_rmse(phone, vols_test, results, mags_predict,
                                 f'{prefix}', 'mags')
