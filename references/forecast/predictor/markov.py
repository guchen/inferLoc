import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import check_X_y, check_array
from sklearn.utils.multiclass import _check_partial_fit_first_call

from forecast.predictor.base import _Seq2SeqPredictorBase


class SklearnMarkovPredictor(BaseEstimator, ClassifierMixin):
    """
    Sklearn style markov predictor
    """

    def __init__(self, order):
        self.order = order
        self.encoder = None
        self.transition_ = None
        self._validate_params()

    def _validate_params(self):
        if not self.order > 0:
            raise ValueError('order muse be >0')

    def _validate_input(self, X, y):
        X, y = check_X_y(X, y, ensure_min_features=self.order)
        if not X.shape[1] == self.order:
            raise ValueError('Columns of X must be equal to order')
        return X, y

    def _validate_input_X(self, X):
        X = check_array(X, ensure_min_features=self.order)
        if not X.shape[1] == self.order:
            raise ValueError('Columns of X must be equal to order')
        return X

    def _transform(self, np_array):
        shape = np_array.shape
        return self.encoder.transform(np_array.reshape(-1, )).reshape(shape)

    def _inverse_transform(self, np_array):
        shape = np_array.shape
        return self.encoder.inverse_transform(np_array.reshape(-1, )).reshape(shape)

    def _partial_fit(self, X, y, classes=None, _refit=False):
        X, y = self._validate_input(X, y)

        if _refit:
            self.classes_ = None

        if _check_partial_fit_first_call(self, classes):
            # This is the first call to partial_fit:
            n_classes = len(self.classes_)
            self.transition_counts_ = np.zeros(
                [n_classes for i in range(self.order + 1)])
            encoder = LabelEncoder().fit(self.classes_)
            self.encoder = encoder

        # convert any classess to int labels: 0,1,2,...
        X = self._transform(X)
        y = self._transform(y)

        n_samples = X.shape[0]

        for i in range(n_samples):
            self.transition_counts_[tuple(X[i])][y[i]] += 1.

        return self._build_transition()

    def _build_transition(self):
        self.transition_ = self.transition_counts_
        return self

    def fit(self, X, y):
        X, y = self._validate_input(X, y)
        labels_X = np.unique(X.reshape(-1, ))
        labels_y = np.unique(y)
        classes = np.union1d(labels_X, labels_y)
        return self._partial_fit(X, y, _refit=True, classes=classes)

    def partial_fit(self, X, y, classes=None):
        return self._partial_fit(X, y, classes=classes)

    def predict(self, X):
        X = self._validate_input_X(X)
        probabilities = self._predict_likelihood(X)
        n_samples = probabilities.shape[0]
        y = [None for i in range(n_samples)]
        for i in range(n_samples):
            counts = probabilities[i, :]
            if np.sum(counts) > 0:
                y[i] = np.argmax(counts)
            else:
                x = self._transform(X[i, :])
                y[i] = x[-1]

        y = np.array(y)
        y = self._inverse_transform(y)
        return y

    def _predict_likelihood(self, X):
        X = self._validate_input_X(X)

        X = self._transform(X)
        probabilities = []
        for i in range(X.shape[0]):
            pos = tuple(X[i])
            probabilities.append(self.transition_[pos])

        return np.array(probabilities)

    def dynamic_score(self, X, y, out_result=False):
        X, y = self._validate_input(X, y)
        n_samples = X.shape[0]
        y_predict = [None for i in range(n_samples)]
        for i in range(n_samples):
            y_predict[i] = self.predict([X[i]])[0]
            self.partial_fit([X[i]], [y[i]])

        if out_result:
            return accuracy_score(y_predict, y), y_predict, y
        else:
            return accuracy_score(y_predict, y)


class Seq2SeqMarkovPredictor(_Seq2SeqPredictorBase):
    """
    seq2seq style markov predictor
    """

    def __init__(self, order, fallback_count=2):
        self.classes_ = None
        self.encoder = {}
        self.order = order
        self.fallback_count = fallback_count
        self.transition_counts = None
        self.transition_counts_cache = {}

    def load(self, transition_counts, encoder):
        self.encoder = encoder
        self.transition_counts = transition_counts

    def bootstrap(self):
        if self.classes_ is None or len(self.classes_) == 0:
            raise EnvironmentError('No classes!')
        # transition_matrix
        order = self.order
        if self.transition_counts is None:
            self.transition_counts = np.zeros(
                [len(self.classes_) for i in range(order + 1)])

    def unique(self, time_series):
        for item in time_series:
            if item not in self.encoder:
                self.encoder[item] = len(self.encoder)

        return list(self.encoder.keys())

    def _2int(self, time_series):
        if hasattr(time_series, '__iter__') and not isinstance(time_series, tuple):
            return [self.encoder[item] for item in time_series]
        else:
            return self._2int([time_series])[0]

    def _2class(self, int_series):
        reverse_encoder = dict(zip(self.encoder.values(), self.encoder.keys()))

        if hasattr(int_series, '__iter__') and not isinstance(int_series, tuple):
            return [reverse_encoder[item] for item in int_series]
        else:
            return self._2class([int_series])[0]

    def _add_transition(self, prefix_int, state_int):
        # clean cache
        self.transition_counts_cache = {}
        if len(prefix_int) != self.order:
            raise Exception(
                'the size of prefix_int must be equal to the order.')
        if self.order > 0:
            self.transition_counts[tuple(prefix_int)][state_int] += 1
        else:
            self.transition_counts[state_int] += 1.

        return self.transition_counts

    def get_transition_counts(self, order):
        if order == self.order:
            return self.transition_counts
        elif 0 <= order < self.order:
            if order in self.transition_counts_cache:
                return self.transition_counts_cache[order]
            else:
                _transition_counts = self.transition_counts
                for _ in range(self.order - order):
                    _transition_counts = _transition_counts.sum(axis=0)
                self.transition_counts_cache[order] = _transition_counts
                return _transition_counts
        else:
            raise ValueError('order must >= 0.')

    def fit(self, time_series):
        if self.classes_ is None:
            self.classes_ = self.unique(time_series)

        self.bootstrap()

        int_series = self._2int(time_series)

        return self._fit_int(int_series)

    def _fit_int(self, int_series):
        # time_series must be list
        order = self.order
        for i in range(order, len(int_series)):
            state_int = int_series[i]
            prefix_int = int_series[i - order:i]
            self._add_transition(prefix_int, state_int)
        return self

    def proba_next(self, prefix=None):
        if prefix is None:
            prefix = []
        if len(prefix) != self.order:
            raise Exception(
                'the size of prefix must be equal to the order. Received: {}, {}'.format(prefix, self.order))

        prefix_int = self._2int(prefix)
        next_state_counts = self._proba_next_int(prefix_int)
        proba = {self._2class(i): next_state_counts[i] for i in range(next_state_counts.shape[0])}
        return proba

    def _proba_next_int(self, prefix_int=None):
        if prefix_int is None:
            prefix_int = []
        if len(prefix_int) != self.order:
            raise Exception(
                'the size of prefix_int must be equal to the order. Received: {}, {}'.format(prefix_int, self.order))
        # predict
        order = self.order
        transition_counts = self.transition_counts
        if order == 0:
            counts = transition_counts
        else:
            counts = transition_counts[tuple(prefix_int)]
            # check if need fallback
            _transition_counts = transition_counts
            _reduced_prefix_int = prefix_int
            _order = self.order
            while counts.sum() <= self.fallback_count and _order > 0:
                _order -= 1
                _transition_counts = _transition_counts.sum(axis=0)
                if _order == 0:
                    counts = _transition_counts
                else:
                    _reduced_prefix_int = _reduced_prefix_int[1:]
                    counts = _transition_counts[tuple(_reduced_prefix_int)]
        return counts

    def fit_predict_proba(self, time_series, n_ratio_predict=0.9):
        if self.classes_ is None:
            self.classes_ = self.unique(time_series)

        self.bootstrap()
        int_series = self._2int(time_series)

        predict_start = None
        if n_ratio_predict < 1:
            predict_start = int(len(int_series) * n_ratio_predict)
        elif n_ratio_predict >= 1:
            predict_start = len(int_series) - n_ratio_predict

        self._fit_int(int_series[0:predict_start])

        Y_proba = []
        for i in range(predict_start, len(int_series)):
            # predict
            prefix_int = int_series[i - self.order:i]
            state_proba = self._proba_next_int(prefix_int)
            proba = {self._2class(_id): state_proba[_id] for _id in range(state_proba.shape[0])}
            Y_proba.append(proba)
            # train increment
            state_int = int_series[i]
            self._add_transition(prefix_int, state_int)
        return Y_proba


class PPMPredictor(Seq2SeqMarkovPredictor):
    def __init__(self, order):
        if order <= 0:
            raise ValueError('Order must > 0')

        super(PPMPredictor, self).__init__(order)
        self.markov_models = [None] * (order + 1)

    def bootstrap(self):
        if self.classes_ is None or len(self.classes_) == 0:
            raise EnvironmentError('No classes!')

        for i in range(self.order + 1):
            model = Seq2SeqMarkovPredictor(order=i)
            model.classes_ = self.classes_
            model.encoder = self.encoder
            model.bootstrap()
            self.markov_models[i] = model

    def _fit_int(self, int_series):
        for k in range(self.order + 1):
            model = self.markov_models[k]
            model._fit_int(int_series[(self.order - k):])

    def _add_transition(self, prefix_int, state_int):
        for k in reversed(range(self.order + 1)):
            model = self.markov_models[k]
            _prefix_int = prefix_int[-k:] if k > 0 else []
            model._add_transition(_prefix_int, state_int)

    def _proba_next_int(self, prefix_int):
        n_classes = len(self.classes_)
        # escape probabilities
        escp_probas = np.ones(self.order + 1)
        _index = list(range(n_classes))
        for k in reversed(range(self.order + 1)):
            model = self.markov_models[k]
            counts = model.transition_counts[tuple(prefix_int[-k:])] if k > 0 else model.transition_counts
            n_counts = counts[_index].sum()
            if n_counts > 0:
                n_unique = counts[counts > 0].shape[0]
                p_escape = n_unique / (n_counts + n_unique) if n_unique < n_classes else 0.
                escp_probas[k] = p_escape
            else:
                escp_probas[k] = 1.
            _index = ~(counts > 0)

        probabilties = np.zeros(n_classes)
        for int_state in range(n_classes):
            _index = list(range(n_classes))
            for k in reversed(range(self.order + 1)):
                model = self.markov_models[k]
                counts = model.transition_counts[tuple(prefix_int[-k:])] if k > 0 else model.transition_counts
                if counts[int_state] > 0:
                    prob = 1 if k == self.order else escp_probas[(k + 1):].prod()
                    counts1 = counts[_index]
                    n_counts = counts1.sum()
                    n_unique = counts[counts > 0].shape[0]
                    prob *= counts[int_state] / (n_counts + n_unique)
                    probabilties[int_state] = prob
                    break
                _index = ~(counts > 0)
        return probabilties
