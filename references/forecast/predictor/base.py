def dictkeymax(dict_counts):
    return max(iter(list(dict_counts.items())), key=lambda item: item[1])[0]


class _Seq2SeqPredictorBase():
    def fit_predict(self, time_series, n_ratio_predict=0.9):
        Y_proba = self.fit_predict_proba(time_series, n_ratio_predict)
        Y_predict = [dictkeymax(proba) for proba in Y_proba]
        return Y_predict

    def fit_predict_proba(self, time_series, n_ratio_predict=0.9):
        raise NotImplementedError

    def predict_next(self, prefix=None):
        counts = self.proba_next(prefix)
        return dictkeymax(counts)

    def proba_next(self, prefix=None):
        raise NotImplementedError
