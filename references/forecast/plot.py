import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import warnings

def cdfplot(data,**kargs):    
    _data = data[~pd.isnull(data)]
    if len(_data)>10**4:
        _data = np.random.choice(data,10**4)
    values = np.sort(_data)    
    def F(x,data):
        return float(len(data[data <= x]))/len(data)

    vF = np.vectorize(F, excluded=['data'])
    
    plt.plot(values,vF(x=values, data=values),**kargs)
    plt.title('CDF')
    plt.ylim(0,1)
    plt.xlim(values.min(),)
    if 'label' in kargs:
        plt.legend(loc=0)

def plot_prediction_results(res):
    plt.figure()
    prefix='accu-'
    for col in res.columns:
        if col[0:len(prefix)] == prefix:
            cdfplot(res[col], label=col[len(prefix):].replace('_','-'))

    if 'active_ratio' in res.columns:
        cdfplot(res['active_ratio'], label='only-idle')

    if 'p-max' in res.columns:
        cdfplot(res['p-max'],label='predictability',linestyle='--')
    else:
        warnings.warn("Warning: No predictability results.")

    
    plt.xlim(0,1)
    plt.xlabel('accuracy')
    plt.show()
    plt.close()
    
#     plt.figure()
#     prefix='rmse-'
#     for col in res.columns:
#         if col[0:len(prefix)] == prefix:
#             cdfplot(res[col], label=col[len(prefix):].replace('_','-'))

#     plt.xlim(1,)
#     plt.xlabel('RMSE')
#     plt.xscale('log')
#     plt.show()
#     plt.close()
    
    prefix='log10rmse-'
    for col in res.columns:
        if col[0:len(prefix)] == prefix:
            cdfplot(res[col], label=col[len(prefix):].replace('_','-'))

    plt.xlabel('Log10 RMSE')
    plt.xlim(0)
    plt.show()
    plt.close()