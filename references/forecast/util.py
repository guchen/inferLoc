import sys
from multiprocessing import Pool as ProcessPool
from multiprocessing import cpu_count, TimeoutError, current_process
from multiprocessing.dummy import Pool as ThreadPool

from sklearn.externals.joblib import Parallel as _Parallel, delayed as _delayed
from sklearn.metrics import accuracy_score, mean_squared_error

from forecast.data import *


def delayed(*args, **kargs):
    return _delayed(*args, **kargs)


def useProcessPool():
    return ('mpi4py' not in sys.modules) and ('MainProcess' in str(current_process()))


def Pool(*args, **kargs):
    if useProcessPool():
        return ProcessPool(*args, **kargs)
    else:
        return ThreadPool(1)


def Parallel(n_jobs=1):
    if useProcessPool():
        return _Parallel(n_jobs=n_jobs)
    else:
        return _Parallel(n_jobs=1)


class AsyncResult(object):
    def __init__(self, apply_async_hooks):
        self.apply_async_hooks = apply_async_hooks

    def get(self):
        result = []
        for hook in self.apply_async_hooks:
            while 1:
                try:
                    result.append(hook.get(0.5))
                    break
                except TimeoutError:
                    continue
        result = np.hstack(result)
        return result


def fair_predict(predict_func, X, Y, n_predict, async=False, **kargs):
    n_samples = X.shape[0]
    batch_size = n_predict
    num_batches = n_samples // batch_size + 1

    Y_predict_all = []
    Y_test_all = []
    #     for k in range(num_batches):
    #         start, end = k * batch_size, (k + 1) * batch_size
    #         ids_test = np.arange(start,end)
    #         ids_train = np.delete(np.arange(n_samples),ids_test)
    #         X_train, Y_train = X[ids_train], Y[ids_train]
    #         X_test, Y_test = X[ids_test], Y[ids_test]

    #         Y_predict = predict_func(X_train,Y_train,X_test, **kargs)

    #         Y_train_all.append(Y_train)
    #         Y_predict_all.append(Y_predict)
    results = []
    pool = Pool(min(cpu_count(), 3))

    for k in range(num_batches):
        start, end = k * batch_size, min((k + 1) * batch_size, n_samples)
        ids_test = np.arange(start, end)
        ids_train = np.delete(np.arange(n_samples), ids_test)
        X_train, Y_train = X[ids_train], Y[ids_train]
        X_test, Y_test = X[ids_test], Y[ids_test]
        Y_test_all.append(Y_test)

        results.append(pool.apply_async(
            predict_func, (X_train, Y_train, X_test), kargs))

    pool.close()
    Y_test_all = np.hstack(Y_test_all)
    if not async:
        pool.join()
        for r in results:
            Y_predict = r.get(1)
            #         Y_predict = predict_func(X_train,Y_train,X_test, **kargs)
            Y_predict_all.append(Y_predict)
        Y_predict_all = np.hstack(Y_predict_all)
    else:
        # print "Async Mode"
        Y_predict_all = AsyncResult(results)

    return Y_predict_all, Y_test_all


def compute_rmse(phone, vols_test, results, y_predict, method, predict):
    vols_predict, mags_predict, log10s_predict = None, None, None
    y_predict = np.array(y_predict)

    if predict == 'mags':
        mags_predict = y_predict
        log10s_predict = mags_predict
        vols_predict = mags_to_vols(mags_predict)
    elif predict == 'log10s':
        log10s_predict = y_predict
        vols_predict = log10s_to_vols(log10s_predict)
        mags_predict = log10s_to_mags(log10s_predict)
    elif predict == 'vols':
        vols_predict = y_predict
        vols_predict[vols_predict < 0] = 0
        mags_predict = vols_to_mags(vols_predict)
        log10s_predict = vols_to_log10s(vols_predict)

    if mags_predict is not None:
        results.loc[phone, 'accu-%s' %
                    method] = accuracy_score(vols_to_mags(vols_test), mags_predict)
    if vols_predict is not None:
        results.loc[phone, 'rmse-%s' %
                    method] = np.sqrt(mean_squared_error(vols_test, vols_predict))
    if log10s_predict is not None:
        results.loc[phone, 'log10rmse-%s' % method] = np.sqrt(
            mean_squared_error(vols_to_log10s(vols_test), log10s_predict))


def compute_accu(phone, seq_test, results, seq_predict, method):
    results.loc[phone, 'accu-%s' % method] = accuracy_score(seq_test, seq_predict)
