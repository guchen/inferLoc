
from math import radians, cos, sin, asin, sqrt


def dist(u, v):
    """Calculate the great circle distance between two points on the earth (specified in decimal degrees)

    Args:
        u: a point in (latitude, longitude)
        v: a point in (latitude, longitude)

    Returns:
        the distance between u and v in kilometers.
    """

    RADIUS_EARTH = 6367  # kilometers

    lat1 = u[0]
    lon1 = u[1]
    lat2 = v[0]
    lon2 = v[1]
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * asin(sqrt(a))
    km = RADIUS_EARTH * c
    return km
