
import numpy as N


def center(pts):
    """get the center of mass of points

    Args:
        pts: a list of points. Each point is in (latitude, longitude)

    Returns:
        a center point in (latitude, longitude)
    """

    lat_mean = N.mean([pt[0] for pt in pts])
    lon_mean = N.mean([pt[1] for pt in pts])

    return (lat_mean, lon_mean)
