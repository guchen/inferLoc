# statistics
from qbmn.geo import dist
from math import sqrt
import scipy.spatial.distance as H
import numpy as N


class KDist(object):
    """the kth closest distance of every points
    input:
        k,
        dist_func: string or function
    """

    def __init__(self, dist_func='euclidean', k=1):
        super(KDist, self).__init__()
        self.dist_func = dist_func
        self.k = k

    def apply(self, points):
        dist_matrices = H.squareform(H.pdist(points, self.dist_func))
        total = len(points)
        k = self.k
        return [N.partition(dist_matrices[i], k)[k] for i in range(0, total)]


class Distance(object):
    """Measure distance between two points (l,t,v)
        l: location rank
        t: time in hours
        v: log10(vol)
    """

    def __init__(self, locations, wl=1, wt=1, wv=1):
        super(Distance, self).__init__()
        self.locations = locations
        self.wl = wl
        self.wt = wt
        self.wv = wv

    def apply_v(self, p1, p2):
        v1 = p1[2]
        v2 = p2[2]
        return 1.0 * abs(v1 - v2) * self.wv

    def apply_l(self, p1, p2):
        l1 = self.locations[p1[0]]
        l2 = self.locations[p2[0]]
        return 1.0 * dist(l1, l2) * self.wl

    def apply_t(self, p1, p2):
        t1 = p1[1]
        t2 = p2[1]
        return 1.0 * abs(t1 - t2) * self.wt

    def apply(self, p1, p2):
        pass

    def diffs(self, p1, p2):
        pass


class ProductDistance(Distance):
    """docstring for ProductDistance"""

    def __init__(self, locations, wl=1, wt=1, wv=1, balance=1):
        super(ProductDistance, self).__init__(locations, wl, wt, wv)
        self.balance = balance

    def apply(self, p1, p2):
        return (self.apply_l(p1, p2) + self.balance) * (self.apply_t(p1, p2) + self.balance) * (self.apply_v(p1, p2) + self.balance)


class EuclideanDistance(Distance):
    """docstring for EuclideanDistance"""

    def __init__(self, locations, wl=1, wt=1, wv=1):
        super(EuclideanDistance, self).__init__(locations, wl, wt, wv)

    def apply(self, p1, p2):
        return sqrt(self.apply_l(p1, p2)**2 + self.apply_t(p1, p2)**2 + self.apply_v(p1, p2) ** 2)
