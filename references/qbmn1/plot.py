
import matplotlib.pyplot as P
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

colors = ['r', 'b', 'g']


def plot_points(ax, points, color='r', alpha=1, **kwargs):
    ax.scatter3D(points[:, 0], points[:, 1], points[:, 2],
                 c=color, alpha=alpha, depthshade=False, edgecolors='none', **kwargs)


def plot_clusters(ax, clusters):
    clusters = sorted(clusters, key=lambda points: len(points), reverse=True)
    i = 0
    while i < len(colors) and i < len(clusters):
        plot_points(ax, clusters[i], color=colors[
                    i], label="Cluster %d" % (i + 1))
        i += 1


def plot_user_clusters(path, clusters, outliers, numloc, points=False, home=-1):
    fig = P.figure()
    ax = fig.gca(projection='3d')

    # outlier
    plot_points(ax, outliers,
                color='k', marker="^", alpha=1, label="Outlier")

    ax.set_xlabel('Location No.')
    ax.set_ylabel('Time (0-24)')
    ax.set_xlim([0, numloc])
    ax.set_ylim([0, 24])  # 1 day
    # ax.set_zscale("log", basez=10)
    ax.set_zlabel('Volume (log10)')
    ax.set_zlim([0, 6])
    # ax.set_zlim([10, max(points[:, 2])])
    plot_clusters(ax, clusters)
    ax.legend(loc="upper left", scatterpoints=1)

    # drawHome
    if home >= 0:
        # zmin = int(min(points[:, 2])) - 1
        # zmax = int(max(points[:, 2])) + 2
        zmin = 6
        zmax = 0
        x = [home, home, home, home]
        y = [0, 24, 24, 0]
        z = [zmin, zmin, zmax, zmax]
        col = Poly3DCollection([zip(x, y, z)], color=(0.1, 0.5, 0.7, 0.1))
        ax.add_collection3d(col)

    fig.savefig(path, bbox_inches='tight')
    P.close()
