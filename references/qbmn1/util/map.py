# Utility functions


_map = map


def map(task, iterable):
    """Apply function to every item of iterable and return a list of the results.

    Same as the built-in map function, with the following advantages:
    1. automatically use multi-processing, when possible
    2. display the progress.
    3. the task is allowed to have multiple input parameters.

    Args:
        task: the task to do
        iterable: parameters for the task

    Returns:
        returns a list consisting of tuples containing the corresponding items from all iterables (a kind of transpose operation)

    """
    pass
