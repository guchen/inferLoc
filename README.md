# Location Inference Tools

This package provides the utility functions to infer missing information in Human Mobility data

## Install
Requirement:
* Python 2.6+, or Python 3.6+
* Pandas, Numpy, Scipy, Tensorly

```shell
git clone https://gitlab.inria.fr/guchen/inferLoc.git
cd inferLoc
python setup.py install
```

## Usage
To be done
